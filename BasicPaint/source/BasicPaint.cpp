﻿// BasicPaint.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "BasicPaint.h"

#define MAX_LOADSTRING 100
#define STATUS_HEIGHT  20

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

HWND hStatusBar;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_BASICPAINT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_BASICPAINT));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_BASICPAINT));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_BASICPAINT);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;

	static HWND hwndEdit;

	// Check menu
	HMENU hMenu = GetMenu(hWnd); // Lấy menu của màn hình chính

	wchar_t szFileName[260] = L"";

	switch (message)
	{
	case WM_CREATE:
	{
					  INITCOMMONCONTROLSEX icc;
					  icc.dwSize = sizeof(icc);
					  icc.dwICC = ICC_WIN95_CLASSES;
					  InitCommonControlsEx(&icc);

					  RECT rcClient;
					  HLOCAL hloc;
					  PINT paParts;
					  int i, nWidth;

					  GetClientRect(hWnd, &rcClient);
					  windowHeight = rcClient.bottom - rcClient.top;
					  windowWidth = rcClient.right - rcClient.left;

					  HFONT hFont = CreateFont(20, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Segoe UI");
					  hStatusBar = CreateWindowEx(0,
						  STATUSCLASSNAME,
						  L"",
						  WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP,
						  0, windowHeight,
						  windowWidth, STATUS_HEIGHT,
						  hWnd, (HMENU)IDC_STATUS_BAR,
						  hInst, NULL);
					  SendMessage(hStatusBar, WM_SETFONT, WPARAM(hFont), TRUE);
					  
					  hloc = LocalAlloc(LHND, sizeof(int)* 3);
					  paParts = (PINT)LocalLock(hloc);

					  //chia status bar 
					  nWidth = rcClient.right / 3;
					  int rightEdge = nWidth;
					  for (i = 0; i < 3; i++) {
						  paParts[i] = rightEdge;
						  rightEdge += nWidth;
					  }

					  // chia làm 3 phần và chèn chữ
					  SendMessage(hStatusBar, SB_SETPARTS, (WPARAM)3, (LPARAM)paParts);
					  SendMessage(hStatusBar, SB_SETTEXT, (WPARAM)0, (LPARAM)L"Simple Paint");
					  LocalUnlock(hloc);
					  LocalFree(hloc);
	}
					  break;
	case WM_LBUTTONDOWN:
	{
						   int x = GET_X_LPARAM(lParam);
						   int y = GET_Y_LPARAM(lParam);

						   if (!isDrawing) {
							   isDrawing = TRUE;
							   firstX = x;
							   firstY = y;

							   switch (curShapeMode)
							   {
							   case DRWLINE:
							   {
											   curShape = new CLine;
							   }
								   break;
							   case DRWRECTANGLE:
							   {
													curShape = new CRectangle;
							   }
								   break;
							   case DRWELLIPSE:
							   {
												  curShape = new CEllipse;
							   }
								   break;
							   }
						   }
	}
		break;
	case WM_MOUSEMOVE:
	{
						 int x = GET_X_LPARAM(lParam);
						 int y = GET_Y_LPARAM(lParam);
						
						 if (isDrawing) {
							 lastX = x;
							 lastY = y;
							 //hiện tọa độ trên Status bar
							 WCHAR buffer[200];
							 wsprintf(buffer, L"%d %d", x, y);
							 SendMessage(hStatusBar, SB_SETTEXT, (WPARAM)1, (LPARAM)buffer);

							 if (GetKeyState(VK_SHIFT) & 0x8000)
							 {
								 min = abs(lastX - firstX) > abs(lastY - firstY) ? abs(lastY - firstY) : abs(lastX - firstX);
								 lastX = firstX < lastX ? firstX + min : firstX - min;
								 lastY = firstY < lastY ? firstY + min : firstY - min;
							 }
							 curShape->SetData(firstX, firstY, lastX, lastY);
						 }		 
						 InvalidateRect(hWnd, NULL, TRUE);
	}
		break;

	case WM_LBUTTONUP:
	{					 
						 if (GetKeyState(VK_SHIFT) & 0x8000)
						 {
							 min = abs(lastX - firstX) > abs(lastY - firstY) ? abs(lastY - firstY) : abs(lastX - firstX);
							 lastX = firstX < lastX ? firstX + min : firstX - min;
							 lastY = firstY < lastY ? firstY + min : firstY - min;
						 }
						 curShape->SetData(firstX, firstY, lastX, lastY);
						 shapes.push_back(curShape);
						 InvalidateRect(hWnd, NULL, TRUE);
						 isDrawing = FALSE;
						 
	}
		break;
	case WM_COMMAND:
	{
					   wmId = LOWORD(wParam);
					   wmEvent = HIWORD(wParam);

					   // Parse the menu selections:
					   switch (wmId)
					   {
					   case ID_DRAW_LINE:
						   curShapeMode = DRWLINE;

						   if (GetMenuState(hMenu, ID_DRAW_RECTANGLE, MF_BYCOMMAND))
							   CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
						   if (GetMenuState(hMenu, ID_DRAW_ELLIPSE, MF_BYCOMMAND))
							   CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);

						   CheckMenuItem(hMenu, ID_DRAW_LINE, MF_CHECKED);
						   break;
					   case ID_DRAW_RECTANGLE:
						   curShapeMode = DRWRECTANGLE;

						   if (GetMenuState(hMenu, ID_DRAW_LINE, MF_BYCOMMAND))
								CheckMenuItem(hMenu, ID_DRAW_LINE, MF_UNCHECKED);
						   if (GetMenuState(hMenu, ID_DRAW_ELLIPSE, MF_BYCOMMAND))
							   CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);
						   CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_CHECKED);

						   break;
					   case ID_DRAW_ELLIPSE:
						   curShapeMode = DRWELLIPSE;

						   if (GetMenuState(hMenu, ID_DRAW_LINE, MF_BYCOMMAND))
							   CheckMenuItem(hMenu, ID_DRAW_LINE, MF_UNCHECKED);
						   if (GetMenuState(hMenu, ID_DRAW_RECTANGLE, MF_BYCOMMAND))
							   CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
						   CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_CHECKED);

						   break;
					   case ID_FILE_NEW:
						   for (int i = shapes.size() - 1; i >= 0; --i)
						   {
							   delete shapes[i];
							   shapes.pop_back();
						   }
						   InvalidateRect(hWnd, NULL, TRUE);
						   break;
					  
					   case ID_FILE_SAVEAS:
					   {
											  OPENFILENAME sfn;
											  int BUFFER_SIZE = 260;
											  ZeroMemory(&sfn, sizeof(sfn));
											  sfn.lStructSize = sizeof(sfn);
											  sfn.hwndOwner = hWnd;
											  sfn.nMaxFile = BUFFER_SIZE;
											  sfn.lpstrFilter = L"Text document (*.txt) \0*.txt\0Bitmap (*.bmp) \0*.bmp\0All files (*.*)\0*.*\0";
											  sfn.lpstrFile = szFileName;
											  sfn.nFilterIndex = 2; //hiển thị ở ô save as type có thứ tự bắt đầu từ 1, ở đây chọn 2 nên hiển thị là bmp
											  sfn.lpstrInitialDir = NULL;
											  sfn.lpstrDefExt = _T("bmp"); //đuôi mặc định
											  sfn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

											  if (!GetSaveFileName(&sfn))
											  {
												  break;
											  }
											  if (sfn.nFilterIndex==1)
												  saveToBinaryFile(sfn.lpstrFile);
											  else {
												  RECT rc;
												  HDC _hdc;
												  GetClientRect(hWnd, &rc);
												  _hdc = GetDC(hWnd);
												  HDC bmpDC = CreateCompatibleDC(NULL);//DC chứa bitmap
												  HBITMAP hbmp = CreateCompatibleBitmap(_hdc, rc.right - rc.left, rc.bottom - rc.top-STATUS_HEIGHT);
												  SelectObject(bmpDC, hbmp);
												  BitBlt(bmpDC, 0, 0, rc.right - rc.left, rc.bottom - rc.top-STATUS_HEIGHT, _hdc, rc.left,rc.top, SRCCOPY);//copy _hdc->bmpDC
												  PBITMAPINFO bmpinf = CreateBitmapInfoStruct(hWnd, hbmp);
												  CreateBMPFile(hWnd, sfn.lpstrFile, bmpinf, hbmp, _hdc);
											  }
					   }
						   break;
					   case ID_FILE_OPEN:
					   {
											OPENFILENAME ofn;
											int BUFFER_SIZE = 260;
											ZeroMemory(&ofn, sizeof(ofn));
											ofn.lStructSize = sizeof(ofn);
											ofn.hwndOwner = hWnd;
											ofn.nMaxFile = BUFFER_SIZE;
											ofn.lpstrFilter = L"Text document (*.txt) \0*.txt\0Bitmap (*.bmp) \0*.bmp\0All files (*.*)\0*.*\0";
											ofn.lpstrFile = szFileName;
											ofn.nFilterIndex = 2; 
											ofn.lpstrInitialDir = NULL;
											ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

											if (!GetOpenFileName(&ofn))
											{
												break;
											}
											if (ofn.nFilterIndex == 1)
												loadFromBinaryFile(szFileName);
											else{
												LoadAndBlitBitmap(hWnd, ofn.lpstrFile);
											}
					   }
						   break;
					   case IDM_ABOUT:
						   DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
						   break;
					   case IDM_EXIT:
						   DestroyWindow(hWnd);
						   break;
					   default:
						   return DefWindowProc(hWnd, message, wParam, lParam);
					   }
	}
		break;
	case WM_PAINT:
	{
					 
					 hdc = BeginPaint(hWnd, &ps);
					 for (int i = 0; i < shapes.size(); i++) {
						 shapes[i]->Draw(hdc);
					 }

					 if (isDrawing) {
						 curShape->SetData(firstX,firstY,lastX,lastY);
						 curShape->Draw(hdc);
					 }
					 
					 EndPaint(hWnd, &ps);
	}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void saveToBinaryFile(wchar_t* filePath)
{
	ofstream out;
	out.open(filePath, ios::out | ios::binary);
	if (out.is_open())
	{
		int n = shapes.size();//ghi số lượng hình
		out.write(reinterpret_cast<const char*>(&n), sizeof(int));

		for (int i = 0; i < n; i++)
		{
			out.write(reinterpret_cast<const char*>(&shapes[i]->type), sizeof(int));
			out.write(reinterpret_cast<const char*>(&shapes[i]->x1), sizeof(int));
			out.write(reinterpret_cast<const char*>(&shapes[i]->y1), sizeof(int));
			out.write(reinterpret_cast<const char*>(&shapes[i]->x2), sizeof(int));
			out.write(reinterpret_cast<const char*>(&shapes[i]->y2), sizeof(int));
		}
	}
	out.close();
}
void loadFromBinaryFile(wchar_t* filePath)
{
	shapes.clear();
	ifstream in;
	in.open(filePath, ios::in | ios::binary);

	int n;//đọc số lượng hình
	if (in.is_open())
	{
		in.read((char*)&n, sizeof(n));
		int x1, x2, y1, y2,type;
		for (int i = 0; i < n; i++)
		{
			in.read((char *)&type, sizeof(int));
			in.read((char*)(&x1), sizeof(int));
			in.read((char*)(&y1), sizeof(int));
			in.read((char*)(&x2), sizeof(int));
			in.read((char*)(&y2), sizeof(int));
			switch (type)
			{
			case DRWLINE:
				curShape = new CLine;
				break;
			case DRWRECTANGLE:
				curShape = new CRectangle;
				break;
			case DRWELLIPSE:
				curShape = new CEllipse;
				break;
			}
			curShape->SetData(x1, y1, x2, y2);
			curShape->Draw(hdc);
			shapes.push_back(curShape);
		}
	}
	in.close();
}

PBITMAPINFO CreateBitmapInfoStruct(HWND hwnd, HBITMAP hBmp)
{
	BITMAP bmp;
	PBITMAPINFO pbmi;
	WORD    cClrBits;

	// Retrieve the bitmap info.  
	if (!GetObject(hBmp, sizeof(BITMAP), (LPSTR)&bmp))
		return NULL;

	// Convert the color format to a count of bits.  
	cClrBits = (WORD)(bmp.bmPlanes * bmp.bmBitsPixel);
	if (cClrBits == 1)
		cClrBits = 1;
	else if (cClrBits <= 4)
		cClrBits = 4;
	else if (cClrBits <= 8)
		cClrBits = 8;
	else if (cClrBits <= 16)
		cClrBits = 16;
	else if (cClrBits <= 24)
		cClrBits = 24;
	else cClrBits = 32;

	// Allocate memory for the BITMAPINFO structure. (This structure  
	// contains a BITMAPINFOHEADER structure and an array of RGBQUAD  
	// data structures.)  

	if (cClrBits < 24)
		pbmi = (PBITMAPINFO)LocalAlloc(LPTR,
		sizeof(BITMAPINFOHEADER)+
		sizeof(RGBQUAD)* (1 << cClrBits));

	// There is no RGBQUAD array for these formats: 24-bit-per-pixel or 32-bit-per-pixel 

	else
		pbmi = (PBITMAPINFO)LocalAlloc(LPTR,
		sizeof(BITMAPINFOHEADER));

	// Initialize the fields in the BITMAPINFO structure.  

	pbmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	pbmi->bmiHeader.biWidth = bmp.bmWidth;
	pbmi->bmiHeader.biHeight = bmp.bmHeight;
	pbmi->bmiHeader.biPlanes = bmp.bmPlanes;
	pbmi->bmiHeader.biBitCount = bmp.bmBitsPixel;
	if (cClrBits < 24)
		pbmi->bmiHeader.biClrUsed = (1 << cClrBits);

	// If the bitmap is not compressed, set the BI_RGB flag.  
	pbmi->bmiHeader.biCompression = BI_RGB;

	// Compute the number of bytes in the array of color  
	// indices and store the result in biSizeImage.  
	// The width must be DWORD aligned unless the bitmap is RLE 
	// compressed. 
	pbmi->bmiHeader.biSizeImage = ((pbmi->bmiHeader.biWidth * cClrBits + 31) & ~31) / 8
		* pbmi->bmiHeader.biHeight;
	// Set biClrImportant to 0, indicating that all of the  
	// device colors are important.  
	pbmi->bmiHeader.biClrImportant = 0;
	return pbmi;
}

void CreateBMPFile(HWND hwnd, LPTSTR pszFile, PBITMAPINFO pbi,HBITMAP hBMP, HDC hDC)
{
	HANDLE hf;                 // file handle  
	BITMAPFILEHEADER hdr;       // bitmap file-header  
	PBITMAPINFOHEADER pbih;     // bitmap info-header  
	LPBYTE lpBits;              // memory pointer  
	DWORD dwTotal;              // total count of bytes  
	DWORD cb;                   // incremental count of bytes  
	BYTE *hp;                   // byte pointer  
	DWORD dwTmp;

	pbih = (PBITMAPINFOHEADER)pbi;
	lpBits = (LPBYTE)GlobalAlloc(GMEM_FIXED, pbih->biSizeImage);

	if (!lpBits)
		return;

	// Retrieve the color table (RGBQUAD array) and the bits  
	// (array of palette indices) from the DIB.  
	if (!GetDIBits(hDC, hBMP, 0, (WORD)pbih->biHeight, lpBits, pbi,
		DIB_RGB_COLORS))
	{
		return;
	}

	// Create the .BMP file.  
	hf = CreateFile(pszFile,
		GENERIC_READ | GENERIC_WRITE,
		(DWORD)0,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		(HANDLE)NULL);
	if (hf == INVALID_HANDLE_VALUE)
		return;
	hdr.bfType = 0x4d42;        // 0x42 = "B" 0x4d = "M"  
	// Compute the size of the entire file.  
	hdr.bfSize = (DWORD)(sizeof(BITMAPFILEHEADER)+
		pbih->biSize + pbih->biClrUsed
		* sizeof(RGBQUAD)+pbih->biSizeImage);
	hdr.bfReserved1 = 0;
	hdr.bfReserved2 = 0;

	// Compute the offset to the array of color indices.  
	hdr.bfOffBits = (DWORD) sizeof(BITMAPFILEHEADER)+
		pbih->biSize + pbih->biClrUsed
		* sizeof (RGBQUAD);

	// Copy the BITMAPFILEHEADER into the .BMP file.  
	if (!WriteFile(hf, (LPVOID)&hdr, sizeof(BITMAPFILEHEADER),
		(LPDWORD)&dwTmp, NULL))
	{
		return;
	}

	// Copy the BITMAPINFOHEADER and RGBQUAD array into the file.  
	if (!WriteFile(hf, (LPVOID)pbih, sizeof(BITMAPINFOHEADER)
		+pbih->biClrUsed * sizeof (RGBQUAD),
		(LPDWORD)&dwTmp, (NULL)))
		return;

	// Copy the array of color indices into the .BMP file.  
	dwTotal = cb = pbih->biSizeImage;
	hp = lpBits;
	if (!WriteFile(hf, (LPSTR)hp, (int)cb, (LPDWORD)&dwTmp, NULL))
		return;

	// Close the .BMP file.  
	if (!CloseHandle(hf))
		return;

	// Free memory.  
	GlobalFree((HGLOBAL)lpBits);
}

void LoadAndBlitBitmap(HWND hwnd, LPCWSTR szFileName)
{
	// Load the bitmap image file
	HBITMAP hBitmap;
	hBitmap = (HBITMAP)::LoadImage(NULL, szFileName, IMAGE_BITMAP, 0, 0,
		LR_LOADFROMFILE);

	// Create a device context that is compatible with the window
	HDC hBMDC;
	hBMDC = ::CreateCompatibleDC(NULL);
	
	// Select the loaded bitmap into the device context
	HBITMAP hOldBmp = (HBITMAP)SelectObject(hBMDC, hBitmap);

	// Get the bitmap's parameters and verify the get
	BITMAP qBitmap;
	GetObject(reinterpret_cast<HGDIOBJ>(hBitmap), sizeof(BITMAP),
		reinterpret_cast<LPVOID>(&qBitmap));

	while (1)
	{
		HDC hdc;
		hdc = GetDC(hwnd);
		// Blit the dc which holds the bitmap onto the window's dc
		BitBlt(hdc, 0, 0, qBitmap.bmWidth, qBitmap.bmHeight,
			hBMDC, 0, 0, SRCCOPY);

		ReleaseDC(hwnd, hdc);
		Sleep(5);
	}
	
	// Unitialize and deallocate resources
	SelectObject(hBMDC, hOldBmp);
	DeleteDC(hBMDC);
	DeleteObject(hBitmap);
}