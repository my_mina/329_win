#pragma once
#include "Shape.h"

class CRectangle:public CShape
{
public:
public:
	void Draw(HDC hdc);

	CShape* Create();

	void SetData(int a, int b, int c, int d);
	CRectangle();
};

