﻿#pragma once
//nhận biết hình cần vẽ

#define DRWLINE 0
#define DRWRECTANGLE 1
# define DRWELLIPSE 2

class CShape
{
public:
	int x1, y1, x2, y2;
	int type;
public:
	virtual void Draw(HDC hdc) = 0;
	virtual CShape* Create() = 0;
	virtual void SetData(int a, int b, int c, int d) = 0;
};

