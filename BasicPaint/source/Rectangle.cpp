#include "stdafx.h"
#include "Rectangle.h"

void CRectangle::Draw(HDC hdc)
{
	Rectangle(hdc, x1, y1, x2, y2);
}

CShape* CRectangle::Create() { 
	return new CRectangle; 
}

void CRectangle::SetData(int a, int b, int c, int d) {
	x1 = a;
	y1 = b;
	x2 = c;
	y2 = d;
}

CRectangle::CRectangle()
{
	this->type = DRWRECTANGLE;
}