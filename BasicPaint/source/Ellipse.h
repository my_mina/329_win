#pragma once
#include "Shape.h"

class CEllipse:public CShape
{
public:
	CEllipse();
	void Draw(HDC hdc);

	CShape* Create();

	void SetData(int a, int b, int c, int d);
};

