#include "stdafx.h"
#include "Ellipse.h"

void CEllipse::Draw(HDC hdc)
{
	Ellipse(hdc, x1, y1, x2, y2);
}

CShape* CEllipse::Create()
{
	return new CEllipse;
}

void CEllipse::SetData(int a, int b, int c, int d)
{
	x1 = a;
	y1 = b;
	x2 = c;
	y2 = d;
}
CEllipse::CEllipse()
{
	this->type = DRWELLIPSE;
}
