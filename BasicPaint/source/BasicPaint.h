﻿#pragma once

#include "resource.h"
#include "Shape.h"
#include "Line.h"
#include "Rectangle.h"
#include "Ellipse.h"
#include <Windowsx.h>
#include <vector>
#include <iostream>
#include <commctrl.h>
#include <Winuser.h>
#include <fstream>
#include <string>
#include <math.h>
#include <Commdlg.h>
#include <objidl.h>
#include <gdiplus.h>
using namespace Gdiplus;
#pragma comment (lib,"Gdiplus.lib")
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")

using namespace std;


//các biến

vector<CShape*> shapes;
vector<CShape*> prototypes;

int curShapeMode;//quản lý chế độ hình muốn vẽ
CShape* curShape = NULL; //hình hiện tại
int  windowHeight, windowWidth;
HDC hdc;
RECT rc;

bool isDrawing = FALSE;
int firstX;
int firstY;
int lastX;
int lastY;
int min;

//các hàm cài đặt
void initNewObject();
void saveToFile(string filePath);
void saveToBinaryFile(wchar_t* filePath);
void loadFromBinaryFile(wchar_t* filePath);
void openFileDialog();
bool SaveBMP(BYTE* Buffer, int width, int height, long paddedsize, LPCTSTR bmpfile);
PBITMAPINFO CreateBitmapInfoStruct(HWND hwnd, HBITMAP hbmp);
void CreateBMPFile(HWND hwnd, LPTSTR pszFile, PBITMAPINFO pbi, HBITMAP hBMP, HDC hDC);
void LoadAndBlitBitmap(HWND hwnd, LPCWSTR szFileName);