#pragma once

#include "resource.h"
#include "DriveHelper.h"
#include "macros.h"
#include <string>
#include <iostream>
#include <fstream>
#include <commctrl.h>
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")
//For StrCpy, StrNCat
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#include <shellapi.h>
#include <stdlib.h>
#include <stdio.h>
#include <Windowsx.h>
#include <Shlobj.h>
#pragma comment(lib, "shell32.lib")
using namespace std;

HWND createListView(long lExtStyle, HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight, long lStyle);
HWND createTreeView(long lExtStyle, HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight, long lStyle);
void loadMyComputerToTree(DriveHelper *drive, HWND m_hTreeView);//load drive and folder to treeview
void loadMyComputerToListView(DriveHelper *drive, HWND m_hListView);//load drive to listview
void loadExpandedChild(HTREEITEM hCurrSelected, HWND m_hTreeView); //Load all child and child of child items in treeview
LPCWSTR getPath(HTREEITEM hItem, HWND m_hTreeView); //Get dir path of an item in Treeview
void loadTreeviewItemAt(HTREEITEM &hParent, LPCWSTR path, HWND m_hTreeView); //Load treeview item at dir path provided
void loadListviewItemAt(LPCWSTR path, HWND m_hParent, HWND m_hListView, DriveHelper *drive); //Load listview item at dir path provided (this function is called when click on a path in treeview)
void loadOrExecSelected(HWND m_hListView); //Load selected directory or execute selected file (used when doubleclick in listview)
void loadDirItemToLisview(HWND m_hParent, HWND m_hListView, LPCWSTR path); //Load directory item to Listview (file/foleder)
void initListviewColumn(HWND m_hListView, int type); //Initialize Listview column (size, header text,...)
LPWSTR convertTimeStampToString(const FILETIME &ftLastWrite); //Convert Active Directory timestamps (LDAP/Win32 FILETIME) to DateTime in String
void WriteSize(HWND hwnd, RECT rc, string s);
void ReadSize(string s, long &l, long &t, long &r, long &b);
RECT rcClient;                       // The parent window's client area.