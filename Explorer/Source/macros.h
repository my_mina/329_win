#define RESET					256
#define WIDTH_ADJUST			2
#define MIN_RESIZABLE_WIDTH		400
#define MIN_RESIZABLE_HEIGHT	350


#define	WINDOW_WIDTH			800
#define	WINDOW_HEIGHT			600

#define LEFT_WINDOW_WIDTH		200
#define	SPLITTER_BAR_WIDTH		2

#define	LEFT_MINIMUM_SPACE		150
#define	RIGHT_MINIMUM_SPACE		500

//Height of the static text is equal to TOP_POS
#define	STATIC_TEXT_HEIGHT		80


//Macros for width and height of the buttons
#define	CLOSE_BUTTON_WIDTH		80

#define	COLOR_BUTTON_WIDTH		250

#define	BUTTON_HEIGHT			26
