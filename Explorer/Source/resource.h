//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MyExplorer.rc
//
#define IDC_MYICON                      2
#define IDD_MYEXPLORER_DIALOG           102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MYEXPLORER                  107
#define IDI_SMALL                       108
#define IDC_MYEXPLORER                  109
#define IDC_TOOLBAR                     113
#define IDT_TREEVIEW                    114
#define IDL_LISTVIEW                    115
#define IDC_STATUSBAR                   116
#define IDR_MAINFRAME                   128
#define IDB_DESKTOP                     129
#define IDB_MYCOMPUTER                  131
#define IDB_FOLDER                      138
#define IDB_DRIVE                       139
#define IDI_ICON1                       140
#define IDI_ICON                        140
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        141
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           117
#endif
#endif
