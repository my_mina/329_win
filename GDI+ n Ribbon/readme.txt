*Thông tin SV
1. MSSV: 1512329
2. Họ tên: Nguyễn Ngọc Xuân Mỹ
3. Email: nnxmy97@gmail.com

*Những chức năng làm được:
- Tạo ribbon.

- Xây dựng chương trình vẽ 5 loại hình cơ bản:
1. Đường thẳng (line). Dùng hàm MoveToEx và LineTo.
2. Hình chữ nhật (rectangle). Dùng hàm Rectangle. Nếu giữ phím Shift sẽ vẽ hình vuông (Square)
3. Hình Ellipse. Dùng hàm Ellipse. Nếu giữ phím Shift sẽ vẽ hình tròn (Circle)

- Cho phép chọn loại hình cần vẽ, chọn nét vẽ, chọn màu vẽ, chọn màu tô.

- Dùng đa xạ để quản lý các đối tượng vẽ hình. Vẽ ở chế độ xem trước.

- New tạo cửa sổ trống mới để vẽ hình.

- Lưu và nạp các đối tượng dạng nhị phân.

- Lưu và nạp các đối tượng dạng bitmap, png, jpg.

*Link youtube demo: https://www.youtube.com/watch?v=7DXScy3RO6U&feature=youtu.be

