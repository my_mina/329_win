﻿#pragma once
#include "resource.h"
#include <ObjIdl.h>
#include <gdiplus.h>
#include <math.h>
#pragma comment(lib, "gdiplus.lib")
using namespace Gdiplus;
//nhận biết hình cần vẽ
#include "Style.h"

#define DRWLINE 0
#define DRWRECTANGLE 1
#define DRWELLIPSE 2

class CShape
{
public:
	int x1, y1, x2, y2;
	int type;
public:
	virtual void Draw(HDC, CStyle) = 0;
	virtual CShape* Create() = 0;
	virtual void SetData(int a, int b, int c, int d) = 0;
};
