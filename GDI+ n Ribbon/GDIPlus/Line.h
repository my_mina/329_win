#pragma once
#include "Shape.h"
class CLine :public CShape
{
public:
public:
	void Draw(HDC, CStyle);
	CShape* Create();
	void SetData(int a, int b, int c, int d);
	CLine();
	~CLine();
};