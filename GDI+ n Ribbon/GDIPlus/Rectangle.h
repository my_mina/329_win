#pragma once
#include "Shape.h"

class CRectangle :public CShape
{
public:
public:
	CRectangle();
	~CRectangle();
	void Draw(HDC, CStyle);

	CShape* Create();

	void SetData(int a, int b, int c, int d);
};

