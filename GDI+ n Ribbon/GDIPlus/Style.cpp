#include "stdafx.h"
#include "Style.h"


CStyle::CStyle()
{
	this->color = color.Black;
	this->size = 1;
	this->check = 0;
}

CStyle::~CStyle()
{
}

void CStyle::SetColor(Color c)
{
	this->color = c;
}

void CStyle::SetSize(int s)
{
	this->size = s;
}