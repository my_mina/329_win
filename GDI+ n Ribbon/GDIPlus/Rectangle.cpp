#include "stdafx.h"
#include "Rectangle.h"

void CRectangle::Draw(HDC hdc, CStyle style)
{
	Graphics* g = new Graphics(hdc);
	g->SetSmoothingMode(SmoothingModeAntiAlias);
	if (!style.check)
	{
		Pen* pen = new Pen(style.color, style.size);
		g->DrawRectangle(pen, (x1 > x2) ? x2 : x1, (y1 > y2) ? y2 : y1, abs(x1 - x2), abs(y1 - y2));
	}
	else{
		SolidBrush sol(style.color);
		g->FillRectangle(&sol, (x1 > x2) ? x2 : x1, (y1 > y2) ? y2 : y1, abs(x1 - x2), abs(y1 - y2));
	}
}

CShape* CRectangle::Create() {
	return new CRectangle;
}

void CRectangle::SetData(int a, int b, int c, int d) {
	x1 = a;
	y1 = b;
	x2 = c;
	y2 = d;
}

CRectangle::CRectangle()
{
	this->type = DRWRECTANGLE;
}


CRectangle::~CRectangle()
{
}