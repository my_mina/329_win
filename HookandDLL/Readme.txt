* Thông tin sinh viên:
1. Họ tên: Nguyễn Ngọc Xuân Mỹ
2. MSSV: 1512329
3. Email: nnxmy97@gmail.com

* Chức năng làm được:
1. Khi kích hoạt với phím tắt bất kì do bạn tự qui định (phím Ctrl), chuột trái và chuột phải bị đổi chỗ cho nhau. Chỉ trở về bình thường khi bấm phím tắt đã định ban đầu.

2. Đưa các hàm xử lí hook vào dll.

*Link youtube: https://www.youtube.com/watch?v=dOYKMO-dTAw&feature=youtu.be