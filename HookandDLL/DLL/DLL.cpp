﻿// DLL.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <Windowsx.h>

#if defined(_WIN32)
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

HHOOK hHook = NULL;
HINSTANCE hinstLib;
bool isblocked = false;

LRESULT CALLBACK MouseHookProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode < 0) // không xử lý message 
		return CallNextHookEx(hHook, nCode, wParam, lParam);
	// xử lý message: Ctrl + Right mouse click
	if (wParam == WM_KEYDOWN && GetAsyncKeyState(VK_CONTROL))
	{
		//TCHAR szBuf[128];
		MOUSEHOOKSTRUCT *mHookData = (MOUSEHOOKSTRUCT *)lParam;
		//wsprintf(szBuf, L"MOUSE – Msg = %d, x = %d, y = %d", wParam, mHookData->pt.x, mHookData->pt.y);
		if (isblocked == false)
		{
			isblocked = true;
			MessageBox(mHookData->hwnd, L"Chuột trái và phải đã bị đổi chỗ cho nhau!\nNhấn Ctrl để trở về bình thường", L"MouseHook", MB_OK);
			SwapMouseButton(TRUE);
			return true;
		}
		else
		{
			MessageBox(mHookData->hwnd, L"Chuột đã trở về trạng thái bình thường!", L"MouseHook", MB_OK);
			SwapMouseButton(FALSE);
			isblocked = false;
			return TRUE;
		}
	}

	return CallNextHookEx(hHook, nCode, wParam, lParam);
}

extern "C" EXPORT void _doInstallHook(HWND hWnd);
void _doInstallHook(HWND hWnd)
{
	if (hHook != NULL) return;

	hHook = SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC)MouseHookProc, hinstLib, 0);
	if (hHook)
		MessageBox(hWnd, L"Setup hook successfully", L"Result", MB_OK);
	else
		MessageBox(hWnd, L"Setup hook fail", L"Result", MB_OK);
}

extern "C" EXPORT void _doRemoveHook(HWND hWnd);
void _doRemoveHook(HWND hWnd)
{
	if (hHook == NULL) return;
	UnhookWindowsHookEx(hHook);
	hHook = NULL;
	MessageBox(hWnd, L"Remove hook successfully", L"Result", MB_OK);
}