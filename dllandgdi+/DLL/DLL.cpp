﻿// DLL.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "DLL.h"

namespace MyPaintLibrary
{
	/*******Cài đặt hàm cho lớp CStyle*******/
	CStyle::CStyle()
	{
		this->color = color.Black;
		this->size = 1;
		this->check = 0;
	}

	CStyle::~CStyle()
	{
	}

	void CStyle::SetColor(Color c)
	{
		this->color = c;
	}

	void CStyle::SetSize(int s)
	{
		this->size = s;
	}

	/*******Cài đặt hàm cho lớp CLine*******/
	void CLine::Draw(HDC hdc, CStyle style)
	{
		Graphics* g = new Graphics(hdc);
		g->SetSmoothingMode(SmoothingModeAntiAlias);

		Pen* pen = new Pen(style.color, style.size);
		g->DrawLine(pen, x1, y1, x2, y2);
	}

	CShape* CLine::Create()
	{
		return new CLine;
	}

	void CLine::SetData(int a, int b, int c, int d)
	{
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;

	}

	CLine::CLine()
	{
		this->type = DRWLINE;
	}

	CLine::~CLine()
	{
	}

	/*******Cài đặt hàm cho lớp CRectangle*******/
	void CRectangle::Draw(HDC hdc, CStyle style)
	{
		Graphics* g = new Graphics(hdc);
		g->SetSmoothingMode(SmoothingModeAntiAlias);
		if (!style.check)
		{
			Pen* pen = new Pen(style.color, style.size);
			g->DrawRectangle(pen, (x1 > x2) ? x2 : x1, (y1 > y2) ? y2 : y1, abs(x1 - x2), abs(y1 - y2));
		}
		else{
			SolidBrush sol(style.color);
			g->FillRectangle(&sol, (x1 > x2) ? x2 : x1, (y1 > y2) ? y2 : y1, abs(x1 - x2), abs(y1 - y2));
		}
	}

	CShape* CRectangle::Create() {
		return new CRectangle;
	}

	void CRectangle::SetData(int a, int b, int c, int d) {
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}

	CRectangle::CRectangle()
	{
		this->type = DRWRECTANGLE;
	}


	CRectangle::~CRectangle()
	{
	}

	/*******Cài đặt hàm cho lớp CEllipse*******/
	void CEllipse::Draw(HDC hdc, CStyle style)
	{
		Graphics* g = new Graphics(hdc);
		g->SetSmoothingMode(SmoothingModeAntiAlias);
		if (!style.check)
		{
			Pen* pen = new Pen(style.color, style.size);
			g->DrawEllipse(pen, (x1 > x2) ? x2 : x1, (y1 > y2) ? y2 : y1, abs(x1 - x2), abs(y1 - y2));
		}
		else{
			SolidBrush sol(style.color);
			g->FillEllipse(&sol, (x1 > x2) ? x2 : x1, (y1 > y2) ? y2 : y1, abs(x1 - x2), abs(y1 - y2));
		}
	}

	CShape* CEllipse::Create()
	{
		return new CEllipse;
	}

	void CEllipse::SetData(int a, int b, int c, int d)
	{
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
	CEllipse::CEllipse()
	{
		this->type = DRWELLIPSE;
	}


	CEllipse::~CEllipse()
	{
	}
}
