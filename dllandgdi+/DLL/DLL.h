﻿#include <ObjIdl.h>
#include <gdiplus.h>
#include <math.h>
#pragma comment(lib, "gdiplus.lib")
using namespace Gdiplus;

#define DRWLINE 0
#define DRWRECTANGLE 1
#define DRWELLIPSE 2

#ifdef MYPAINTLIBRARY_EXPORTS  
#define MYPAINTLIBRARY_API __declspec(dllexport)   
#else  
#define MYPAINTLIBRARY_API __declspec(dllimport)   
#endif

namespace MyPaintLibrary
{
	class CStyle
	{
	public:
		Color color;
		int size; //độ dày của chữ
		short check;
	public:
		MYPAINTLIBRARY_API CStyle();
		MYPAINTLIBRARY_API ~CStyle();
		MYPAINTLIBRARY_API void SetColor(Color c);
		MYPAINTLIBRARY_API void SetSize(int s);
	};

	class CShape
	{
	public:
		int x1, y1, x2, y2;
		int type;
	public:
		MYPAINTLIBRARY_API virtual void Draw(HDC, CStyle) = 0;
		MYPAINTLIBRARY_API virtual CShape* Create() = 0;
		MYPAINTLIBRARY_API virtual void SetData(int a, int b, int c, int d) = 0;
	};

	class CRectangle :public CShape
	{
	public:
	public:
		MYPAINTLIBRARY_API CRectangle();
		MYPAINTLIBRARY_API ~CRectangle();
		MYPAINTLIBRARY_API void Draw(HDC, CStyle);

		MYPAINTLIBRARY_API CShape* Create();

		MYPAINTLIBRARY_API void SetData(int a, int b, int c, int d);
	};

	class CLine :public CShape
	{
	public:
	public:
		MYPAINTLIBRARY_API void Draw(HDC, CStyle);
		MYPAINTLIBRARY_API CShape* Create();
		MYPAINTLIBRARY_API void SetData(int a, int b, int c, int d);
		MYPAINTLIBRARY_API CLine();
		MYPAINTLIBRARY_API ~CLine();
	};

	class CEllipse :public CShape
	{
	public:
		MYPAINTLIBRARY_API CEllipse();
		MYPAINTLIBRARY_API void Draw(HDC, CStyle);

		MYPAINTLIBRARY_API CShape* Create();

		MYPAINTLIBRARY_API void SetData(int a, int b, int c, int d);
		MYPAINTLIBRARY_API ~CEllipse();
	};
}