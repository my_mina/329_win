﻿#pragma once

#include "resource.h"
#include "DLL.h"
#include <Windowsx.h>
#include <vector>
#include <iostream>
#include <commctrl.h>
#include <Winuser.h>
#include <fstream>
#include <string>
#include <math.h>
#include <Commdlg.h>
#include <objidl.h>
#include <gdiplus.h>
using namespace Gdiplus;
#pragma comment (lib,"Gdiplus.lib")
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")

using namespace std;


//các biến
RECT rc;
Bitmap *gpBitmap;
bool isImage = false;
Graphics *grp;

vector<MyPaintLibrary::CShape*> shapes;
int curShapeMode;//quản lý chế độ hình muốn vẽ
MyPaintLibrary::CShape* curShape = NULL; //hình hiện tại

//style
vector<MyPaintLibrary::CStyle> style;
MyPaintLibrary::CStyle curStyle;

bool isDrawing = FALSE;
int firstX;
int firstY;
int lastX;
int lastY;
int min;

//các hàm cài đặt
void initNewObject();
void saveToFile(string filePath);
void saveToBinaryFile(wchar_t* filePath);
void loadFromBinaryFile(wchar_t* filePath, HWND);
void openFileDialog();
int GetEncoderClsid(const WCHAR* format, CLSID* pClsid);

UINT RIBBON_HEIGHT = 150;