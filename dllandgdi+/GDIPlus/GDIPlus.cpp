﻿// GDIPlus.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "GDIPlus.h"
#include <Objbase.h>
#pragma comment(lib, "Ole32.lib")
#include "RibbonFramework.h"
#include "RibbonIDs.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPTSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	HRESULT hr = CoInitialize(NULL);
	if (FAILED(hr))
	{
		return FALSE;
	}

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_GDIPLUS, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	CoUninitialize();

	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = 0;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_GDIPLUS));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCE(IDC_GDIPLUS);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // Store instance handle in our global variable

	hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId;
	bool initSuccess;

	wchar_t szFileName[260] = L"";

	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	switch (message)
	{
	case WM_CREATE:
		// Initializes the Ribbon framework.
		initSuccess = InitializeFramework(hWnd);
		if (!initSuccess)
		{
			return -1;
		}

		break;
	case WM_COMMAND:
	{
					   wmId = LOWORD(wParam);
					   // Parse the menu selections:
					   switch (wmId)
					   {
					   case ID_DRAW_LINE:
						   curShapeMode = DRWLINE;
						   break;
					   case ID_DRAW_RECTANGLE:
						   curShapeMode = DRWRECTANGLE;
						   break;
					   case ID_DRAW_ELLIPSE:
						   curShapeMode = DRWELLIPSE;
						   break;
					   case ID_SIZE_1:
						   curStyle.SetSize(1);
						   break;
					   case ID_SIZE_3:
						   curStyle.SetSize(3);
						   break;
					   case ID_SIZE_5:
						   curStyle.SetSize(5);
						   break;
					   case ID_SIZE_8:
						   curStyle.SetSize(8);
						   break;
					   case ID_CO_RED:
						   curStyle.check = 0;
						   curStyle.color = Color::Red;
						   break;
					   case ID_CO_ORANGE:
						   curStyle.check = 0;
						   curStyle.color = Color::Orange;
						   break;
					   case ID_CO_YELLOW:
						   curStyle.check = 0;
						   curStyle.color = Color::Yellow;
						   break;
					   case ID_CO_GREEN:
						   curStyle.check = 0;
						   curStyle.color = Color::Green;
						   break;
					   case ID_CO_BLUE:
						   curStyle.check = 0;
						   curStyle.color = Color::Blue;
						   break;
					   case ID_CO_INDIGO:
						   curStyle.check = 0;
						   curStyle.color = Color::Indigo;
						   break;
					   case ID_CO_VIOLET:
						   curStyle.check = 0;
						   curStyle.color = Color::Violet;
						   break;
					   case ID_BRU_RED:
						   curStyle.check = 1;
						   curStyle.color = Color::Red;
						   break;
					   case ID_BRU_ORANGE:
						   curStyle.check = 1;
						   curStyle.color = Color::Orange;
						   break;
					   case ID_BRU_YELLOW:
						   curStyle.check = 1;
						   curStyle.color = Color::Yellow;
						   break;
					   case ID_BRU_GREEN:
						   curStyle.check = 1;
						   curStyle.color = Color::Green;
						   break;
					   case ID_BRU_BLUE:
						   curStyle.check = 1;
						   curStyle.color = Color::Blue;
						   break;
					   case ID_BRU_INDIGO:
						   curStyle.check = 1;
						   curStyle.color = Color::Indigo;
						   break;
					   case ID_BRU_VIOLET:
						   curStyle.check = 1;
						   curStyle.color = Color::Violet;
						   break;
					   case ID_NO_COL:
						   curStyle.check = 0;
						   curStyle.color = Color::Black;
						   break;
					   case ID_FILE_NEW:
						   shapes.clear();
						   style.clear();
						   InitializeFramework(hWnd);
						   InvalidateRect(hWnd, NULL, TRUE);
						   break;
					   case ID_FILE_SAVEAS:
					   {
											  OPENFILENAME sfn;
											  int BUFFER_SIZE = 260;
											  ZeroMemory(&sfn, sizeof(sfn));
											  sfn.lStructSize = sizeof(sfn);
											  sfn.hwndOwner = hWnd;
											  sfn.nMaxFile = BUFFER_SIZE;
											  sfn.lpstrFilter = L"Text document (*.txt) \0*.txt\0Bitmap (*.bmp) \0*.bmp\0PNG (*.png) \0*.png\0JPEG (*.jpg) \0*.jpg\0All files (*.*)\0*.*\0";
											  sfn.lpstrFile = szFileName;
											  sfn.nFilterIndex = 5; //hiển thị ở ô save as type có thứ tự bắt đầu từ 1
											  sfn.lpstrInitialDir = NULL;
											  sfn.lpstrDefExt = _T("txt"); //đuôi mặc định
											  sfn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT;

											  if (!GetSaveFileName(&sfn))
											  {
												  break;
											  }

											  if (sfn.nFilterIndex == 1)
												  saveToBinaryFile(sfn.lpstrFile);
											  else {
												  RECT rect;
												  HDC _hdc;
												  GetClientRect(hWnd, &rect);
												  _hdc = GetDC(hWnd);
												  HDC bmpDC = CreateCompatibleDC(NULL);//DC chứa bitmap
												  HBITMAP hbmp = CreateCompatibleBitmap(_hdc, rect.right - rect.left, rect.bottom - rect.top - RIBBON_HEIGHT);
												  SelectObject(bmpDC, hbmp);
												  BitBlt(bmpDC, 0, 0, rect.right - rect.left, rect.bottom - rect.top - RIBBON_HEIGHT, _hdc, rect.left, RIBBON_HEIGHT, SRCCOPY);
												  InvalidateRect(hWnd, NULL, TRUE);
												  Bitmap img(hbmp, NULL);
												  if (sfn.nFilterIndex == 3){
													  CLSID pngcls;
													  GetEncoderClsid(L"image/png", &pngcls);
													  img.Save(sfn.lpstrFile, &pngcls, NULL);
												  }
												  else if (sfn.nFilterIndex == 4){
													  CLSID jpgcls;
													  GetEncoderClsid(L"image/jpeg", &jpgcls);
													  img.Save(sfn.lpstrFile, &jpgcls, NULL);
												  }
												  else {
													  CLSID bmpcls;
													  GetEncoderClsid(L"image/bmp", &bmpcls);
													  img.Save(sfn.lpstrFile, &bmpcls, NULL);
												  }
											  }
					   }
						   break;
					   case ID_FILE_OPEN:
					   {
											OPENFILENAME ofn;
											int BUFFER_SIZE = 260;
											ZeroMemory(&ofn, sizeof(ofn));
											ofn.lStructSize = sizeof(ofn);
											ofn.hwndOwner = hWnd;
											ofn.nMaxFile = BUFFER_SIZE;
											ofn.lpstrFilter = L"Text document (*.txt) \0*.txt\0Bitmap (*.bmp) \0*.bmp\0PNG (*.png) \0*.png\0JPEG (*.jpg) \0*.jpg\0All files (*.*)\0*.*\0";
											ofn.lpstrFile = szFileName;
											ofn.nFilterIndex = 5;
											ofn.lpstrInitialDir = NULL;
											ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

											if (!GetOpenFileName(&ofn))
											{
												break;
											}
											if (ofn.nFilterIndex == 1)
												loadFromBinaryFile(szFileName, hWnd);

											else if (ofn.nFilterIndex == 2 || ofn.nFilterIndex == 3 || ofn.nFilterIndex == 4)
											{

												gpBitmap = new Bitmap(szFileName);
												isImage = true;
												// We need to force the window to redraw itself
												InvalidateRect(hWnd, NULL, TRUE);
												//UpdateWindow(hWnd);
											}
					   }
					   }
	}
		break;
	case WM_LBUTTONDOWN:
	{
						   int x = GET_X_LPARAM(lParam);
						   int y = GET_Y_LPARAM(lParam);

						   if (!isDrawing) {
							   isDrawing = TRUE;
							   firstX = x;
							   firstY = y;

							   switch (curShapeMode)
							   {
							   case DRWLINE:
								   curShape = new MyPaintLibrary::CLine;
								   break;
							   case DRWRECTANGLE:
								   curShape = new MyPaintLibrary::CRectangle;
								   break;
							   case DRWELLIPSE:
								   curShape = new MyPaintLibrary::CEllipse;
								   break;
							   }
						   }
	}
		break;
	case WM_MOUSEMOVE:
	{
						 int x = GET_X_LPARAM(lParam);
						 int y = GET_Y_LPARAM(lParam);

						 if (isDrawing) {
							 lastX = x;
							 lastY = y;

							 if (GetKeyState(VK_SHIFT) & 0x8000)
							 {
								 min = abs(lastX - firstX) > abs(lastY - firstY) ? abs(lastY - firstY) : abs(lastX - firstX);
								 lastX = firstX < lastX ? firstX + min : firstX - min;
								 lastY = firstY < lastY ? firstY + min : firstY - min;
							 }
							 curShape->SetData(firstX, firstY, lastX, lastY);
							 InvalidateRect(hWnd, NULL, FALSE);
						 }

	}
		break;

	case WM_LBUTTONUP:
	{
						 if (lastX != firstX | lastY != firstY)
						 {

							 if (GetKeyState(VK_SHIFT) & 0x8000)
							 {
								 min = abs(lastX - firstX) > abs(lastY - firstY) ? abs(lastY - firstY) : abs(lastX - firstX);
								 lastX = firstX < lastX ? firstX + min : firstX - min;
								 lastY = firstY < lastY ? firstY + min : firstY - min;
							 }
							 curShape->SetData(firstX, firstY, lastX, lastY);
							 shapes.push_back(curShape);
							 style.push_back(curStyle);
							 InvalidateRect(hWnd, NULL, FALSE);
						 }

						 isDrawing = FALSE;
	}
		break;
	case WM_PAINT:
	{
					 PAINTSTRUCT ps;
					 HDC hdc;

					 hdc = BeginPaint(hWnd, &ps);

					 RECT rect;
					 GetClientRect(hWnd, &rect);
					 // Tạo MDC tương thích với DC
					 HDC hMemDC;
					 hMemDC = CreateCompatibleDC(hdc);

					 // Select the loaded bitmap into the device context
					 HBITMAP hOldBmp = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
					 SelectObject(hMemDC, hOldBmp);

					 FillRect(hMemDC, &rect, HBRUSH(GetBkColor(hMemDC)));	// Vẽ lại nền
					 grp = new Graphics(hMemDC);		// Vẽ trên nền MemDC
					 grp->SetSmoothingMode(SmoothingModeAntiAlias);

					 if (isImage)
					 {
						 grp->DrawImage(gpBitmap, 0, RIBBON_HEIGHT);
						 isImage = false;
					 }
					 else
					 {
						 for (int i = 0; i < shapes.size(); i++)
							 shapes[i]->Draw(hMemDC, style[i]);
						 if (isDrawing){
							 curShape->Draw(hMemDC, curStyle);
						 }
					 }

					 BitBlt(hdc, 0, 0, rect.right, rect.bottom, hMemDC, 0, 0, SRCCOPY);

					 SelectObject(hMemDC, hOldBmp);
					 DeleteDC(hMemDC);
					 DeleteObject(hOldBmp);
					 delete grp;

					 EndPaint(hWnd, &ps);
	}
		break;
	case WM_DESTROY:
		DestroyFramework();
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void saveToBinaryFile(wchar_t* filePath)
{
	ofstream out;
	out.open(filePath, ios::out | ios::binary);
	if (out.is_open())
	{
		int n = shapes.size();//ghi số lượng hình
		out.write(reinterpret_cast<const char*>(&n), sizeof(int));

		for (int i = 0; i < n; i++)
		{
			out.write(reinterpret_cast<const char*>(&shapes[i]->type), sizeof(int));
			out.write(reinterpret_cast<const char*>(&shapes[i]->x1), sizeof(int));
			out.write(reinterpret_cast<const char*>(&shapes[i]->y1), sizeof(int));
			out.write(reinterpret_cast<const char*>(&shapes[i]->x2), sizeof(int));
			out.write(reinterpret_cast<const char*>(&shapes[i]->y2), sizeof(int));
			out.write(reinterpret_cast<const char*>(&style[i].check), sizeof(short));
			out.write(reinterpret_cast<const char*>(&style[i].color), sizeof(Color));
			out.write(reinterpret_cast<const char*>(&style[i].size), sizeof(int));
		}
	}
	out.close();
}
void loadFromBinaryFile(wchar_t* filePath, HWND hwnd)
{
	ifstream in;
	in.open(filePath, ios::in | ios::binary);

	shapes.clear();
	style.clear();

	HDC _hdc = GetDC(hwnd);
	int n;//đọc số lượng hình
	if (in.is_open())
	{
		in.read((char*)&n, sizeof(n));
		int type;

		for (int i = 0; i < n; i++)
		{
			MyPaintLibrary::CStyle tmp;
			in.read((char *)&type, sizeof(int));
			in.read((char*)&firstX, sizeof(int));
			in.read((char*)&firstY, sizeof(int));
			in.read((char*)&lastX, sizeof(int));
			in.read((char*)&lastY, sizeof(int));
			in.read((char*)&curStyle.check, sizeof(short));
			in.read((char*)&curStyle.color, sizeof(Color));
			in.read((char*)&curStyle.size, sizeof(int));
			switch (type)
			{
			case DRWLINE:
				curShape = new MyPaintLibrary::CLine;
				break;
			case DRWRECTANGLE:
				curShape = new MyPaintLibrary::CRectangle;
				break;
			case DRWELLIPSE:
				curShape = new MyPaintLibrary::CEllipse;
				break;
			}
			curShape->SetData(firstX, firstY, lastX, lastY);
			curShape->Draw(_hdc, curStyle);
			shapes.push_back(curShape);
			style.push_back(curStyle);
		}
		InvalidateRect(hwnd, NULL, TRUE);
	}
	in.close();
}

int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
	/*Reference: Retrieving the Class Identifier for an Encoder (msdn)*/

	UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes

	ImageCodecInfo* pImageCodecInfo = NULL;

	GetImageEncodersSize(&num, &size);
	if (size == 0)
		return -1;  // Failure

	pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
	if (pImageCodecInfo == NULL)
		return -1;  // Failure

	GetImageEncoders(num, size, pImageCodecInfo);

	for (UINT j = 0; j < num; ++j)
	{
		if (wcscmp(pImageCodecInfo[j].MimeType, format) == 0)
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;  // Success
		}
	}

	free(pImageCodecInfo);
	return -1;  // Failure
}