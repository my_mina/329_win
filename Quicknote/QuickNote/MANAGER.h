#pragma once
#include "stdafx.h"
#include "NOTE.h"
#include "TAG.h"
#include <sstream> 
#include <fstream>
#include <locale>
#include <codecvt>

using namespace std;

#define NOTE_DATA_FOLDER	L"NoteData/"
#define TAG_PATH			L"TagData/Tags.txt"

class MANAGER
{
public:
	vector<NOTE> NoteList;
	vector<TAG> TagList;

	wstring standardizedTag(wstring tag)
	{
		while (tag.size() > 0)
		{
			if (tag[0] == ' ')
			{
				tag.erase(0, 1);
			}
			else
			{
				break;
			}
		}

		while (tag.size() > 1)
		{
			if (tag[tag.size() - 1] == ' ')
			{
				tag.pop_back();
			}
			else
			{
				break;
			}
		}

		return tag;
	}

	vector<int> strToTagList(wstring rawTag)
	{
		vector<int> tagList;
		wstringstream ssRawTag(rawTag);
		wstring singleTag;

		while (getline(ssRawTag, singleTag, L','))
		{
			tagList.push_back(stoi(singleTag));
		}
		return tagList;
	}

	//remove first element
	wstring TagListToString(vector<wstring> tagList)
	{
		wstring tagString;
		if (tagList.size() > 1)
		{
			for (int i = 1; i < tagList.size(); i++)
			{
				tagString += tagList[i] + L", ";
			}
		}
		return tagString;

	}

	//save a note to NoteList and TagList
	int saveNoteToList(wstring rawTag, wstring content)
	{
		//save tags
		bool existed = false;
		vector<int> vTagID;

		wstring singleTag;
		wstringstream ssRawTag(rawTag);
		while (getline(ssRawTag, singleTag, L','))
		{
			singleTag = standardizedTag(singleTag);

			if (singleTag != L"")
			{
				for (int i = 0; i < TagList.size(); i++)
				{
					//tag is existed
					if (singleTag == TagList[i].TagName)
					{
						TagList[i].Id.push_back(NoteList.size());
						vTagID.push_back(i);
						existed = true;
						break;
					}
				}
				//tag is not existed
				if (existed == false)
				{
					vTagID.push_back(TagList.size());
					TAG newTag(singleTag, NoteList.size());
					TagList.push_back(newTag);
				}
			}
		}

		//Save note
		NOTE newNote(NoteList.size(), vTagID, content);
		NoteList.push_back(newNote);

		return 0;
	}

	//becareful cos this fuction "rewrite" all data, not "update"
	int saveToFile()
	{
		//save Tags data
		wofstream fileTags;
		wofstream fileNotes;
		fileTags.open(TAG_PATH);

		if (!fileTags.is_open())
		{
			return 1;
		}

		fileTags.imbue(locale(locale::empty(), new codecvt_utf8<wchar_t>));
		for (int i = 0; i < TagList.size(); i++)
		{
			fileTags << TagList[i].TagName << endl;
			for (int j = 0; j < TagList[i].Id.size(); j++)
			{
				fileTags << TagList[i].Id[j] << " ";
			}
			fileTags << endl;
		}
		fileTags.close();

		//write notes
		
		wstringstream ss;
		//ss << NOTE_DATA_FOLDER << NoteList[i].Id << ".txt";
		int i = NoteList.size()-1;
		ss << NOTE_DATA_FOLDER << i << ".txt";
		fileNotes.open(ss.str());
		if (!fileNotes.is_open())
		{
			return 2;
		}

		fileNotes.imbue(locale(locale::empty(), new codecvt_utf8<wchar_t>));

		for (int j = 0; j < NoteList[i].vTags.size(); j++)
		{
			fileNotes << NoteList[i].vTags[j] << ",";
		}
		fileNotes << endl;
		fileNotes << NoteList[i].Content;

		fileNotes.close();

		return 0;
	}

	int loadFromFile()
	{
		if (CreateDirectory(NOTE_DATA_FOLDER, NULL) ||
			ERROR_ALREADY_EXISTS == GetLastError())
		{
			// CopyFile(...)
		}
		else
		{
			// Failed to create directory.
		}

		if (CreateDirectory(L"TagData", NULL) ||
			ERROR_ALREADY_EXISTS == GetLastError())
		{
			// CopyFile(...)
		}
		else
		{
			// Failed to create directory.
		}

		TagList.resize(0);
		NoteList.resize(0);

		wfstream fileTag;
		fileTag.open(TAG_PATH);
		if (!fileTag.is_open())
		{
			return 1;
		}
		fileTag.imbue(locale(locale::empty(), new codecvt_utf8<wchar_t>));

		wstring tagName, id, idListStr;
		wstringstream  idList;
		while (getline(fileTag, tagName))
		{
			getline(fileTag, idListStr);
			//copy to stringstream to extract id
			std::wstringstream().swap(idList);
			idList << idListStr;

			TAG currTag(tagName);
			while (getline(idList, id, L' '))
			{
				currTag.Id.push_back(wcstod(id.c_str(), NULL));
			}

			TagList.push_back(currTag);
		}
		fileTag.close();


		//read notes file
		wfstream fileNote;

		HANDLE hFind;
		WIN32_FIND_DATA data;

		wstringstream pathSS, tempSS;
		wstring tagsString, tempStr;
		pathSS << NOTE_DATA_FOLDER << "*.txt";
		hFind = FindFirstFile(pathSS.str().c_str(), &data);
		if (hFind != INVALID_HANDLE_VALUE) {
			do
			{
				wstringstream().swap(pathSS);
				pathSS << NOTE_DATA_FOLDER << data.cFileName;
				fileNote.open(pathSS.str());
				if (!fileNote.is_open())
				{
					return 2;
				}


				fileNote.imbue(locale(locale::empty(), new codecvt_utf8<wchar_t>));
				getline(fileNote, tagsString);

				wstringstream().swap(tempSS);//reset stringstream
				while (getline(fileNote, tempStr))
				{
					tempSS << tempStr << "\r\n";
				}

				id = data.cFileName;
				id[id.size() - 4] = '\0'; //delete ".txt"
				NOTE currNote(wcstol(id.c_str(), NULL, 10), strToTagList(tagsString), tempSS.str(), data.ftLastWriteTime);
				NoteList.push_back(currNote);

				fileNote.close();
			} while (FindNextFile(hFind, &data));
			FindClose(hFind);
		}

		return 0;
	}
};