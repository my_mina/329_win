#pragma once

#include "resource.h"
#include <Windowsx.h>
#include "MANAGER.h"
#include <vector>
#include <algorithm> 
#include <iomanip>
#include "Chart.h"
#include <commctrl.h>
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")

using namespace std;

//My variables
HWND hWndEdtNote;
HWND hWndBtnSave;
HWND hWndCBBTag;
//View note dialog Hwnd
HWND hTagList, hNoteList, hContentNote;
HWND hWndTagsList, hWndNotesList, hWndContNote;
HWND hCurNote, hTag;
MANAGER *Manager;

wstring edtTagStr;
bool updateTagStr = false;
Chart* mChart;

//My prototypes
int updateLBTag(MANAGER *Manager, HWND hWndLB);
int updateViewNotes(MANAGER *Manager, HWND hWndLB,int);
int updateContNote(HWND hWndEdtNote, NOTE note);
int updateCombox(MANAGER *Manager, HWND hWndLB);
int getSltedTagPos(MANAGER *Manager, HWND hWndTagList);
int getSltedNoteID(MANAGER *Manager, HWND hWndNoteList,int);
static RECT createRect(LONG left, LONG top, LONG right, LONG bottom);
bool sortTagList(TAG tag1, TAG tag2);
void doInstallHook(HWND hWnd);
void doRemoveHook(HWND hWnd);
wstring GetTextboxWindow(HWND);
HWND ListNotesOfATag(HWND, HINSTANCE, long ID);
HWND TagOfANote(HWND hDlg, HINSTANCE hInst, long ID);
LPWSTR convertTimeStampToString(const FILETIME &ftLastWrite);

INT_PTR CALLBACK ViewNotesWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK ViewStatisticsWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);