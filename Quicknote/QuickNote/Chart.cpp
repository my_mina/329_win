﻿#include "stdafx.h"
#include "Chart.h"


Chart::Chart()
{
	this->total = 0;
}

Chart::Chart(Coord co, int r)
{
	this->origin.x = co.x;
	this->origin.y = co.y;
	this->radius = r;

	this->listTags.resize(6);
	this->listTags[0].color = { 231, 76, 60 };
	this->listTags[1].color = { 52, 152, 219 };
	this->listTags[2].color = { 39, 174, 96 };
	this->listTags[3].color = { 142, 68, 173 };
	this->listTags[4].color = { 211, 84, 0 };
	this->listTags[5].color = { 44, 62, 80 };

	this->listTags[0].amount = 0;
	this->listTags[1].amount = 0;
	this->listTags[2].amount = 0;
	this->listTags[3].amount = 0;
	this->listTags[4].amount = 0;
	this->listTags[5].amount = 0;
}

float Chart::GetSweepAngle(int index)
{
	float percent = (float)this->listTags[index].amount / this->total;
	return float(percent * 360);
}

int Chart::GetTotal(vector<TAG> vTags)
{
	int total = 0;
	for (int i = 0; i < vTags.size(); i++)
	{
		total += vTags[i].Id.size();
	}
	this->total = total;
	return total;
}

void Chart::Draw(HDC hdc)
{
	for (int i = 0; i < this->listTags.size(); ++i)
	{
		if (this->listTags[i].amount > 0)
		{
			SetDCBrushColor(hdc, RGB(this->listTags[i].color.r, this->listTags[i].color.g, this->listTags[i].color.b));
			SetDCPenColor(hdc, RGB(241, 241, 241));
			SelectObject(hdc, GetStockObject(DC_BRUSH));
			SelectObject(hdc, GetStockObject(DC_PEN));
			//tham khảo drawing a pie chart trên msdn
			BeginPath(hdc);
			MoveToEx(hdc, this->origin.x, this->origin.y, NULL);
			AngleArc(hdc, this->origin.x, this->origin.y, this->radius, this->listTags[i].xStartAngle, this->listTags[i].xSweepAngle);
			LineTo(hdc, this->origin.x, this->origin.y);
			EndPath(hdc);
			StrokeAndFillPath(hdc); //vẽ viền và tô màu bằng màu hiện tại cho cung tròn
		}
	}
}

void Chart::UpdateCoord(vector<TAG>Tags)
{
	//cập nhật tổng số note trong một tag
	int total = this->GetTotal(Tags);
	if (Tags.size() <= 5)
	{
		for (int i = 0; i < Tags.size(); ++i)
		{
			this->listTags[i].name = Tags[i].TagName;
			this->listTags[i].amount = Tags[i].Id.size();
		}
	}
	else
	{
		int first5Tags = 0;
		for (int i = 0; i < 5; ++i)
		{
			this->listTags[i].name = Tags[i].TagName;
			this->listTags[i].amount = Tags[i].Id.size();
			first5Tags += this->listTags[i].amount;
		}
		this->listTags[5].name = L"Các tag còn lại";
		this->listTags[5].amount = total - first5Tags;
	}
	//cập nhật góc của từng tag
	for (int i = 0; i < this->listTags.size(); ++i)
	{
		this->listTags[i].xSweepAngle = this->GetSweepAngle(i);
	}

	this->listTags[0].xStartAngle = 90;

	for (int i = 1; i < this->listTags.size(); ++i)
	{
		this->listTags[i].xStartAngle = this->listTags[i - 1].xStartAngle + this->listTags[i - 1].xSweepAngle;
	}
}
void Chart::ShowNote(HDC hdc)
{
	HFONT hFontInside = CreateFont(20, 0, 0, 0, 600, FALSE, FALSE, FALSE, VIETNAMESE_CHARSET,
		OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Calibri"));
	HFONT hFontOutside = CreateFont(20, 0, 0, 600, FW_DONTCARE, FALSE, FALSE, FALSE, VIETNAMESE_CHARSET,
		OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Calibri"));
	vector<Coord> CorOfPie = {
		{ 20, 350 },
		{ 200, 350 },
		{ 400, 350 },
		{ 20, 430 },
		{ 200, 430 },
		{ 400, 430 }
	};
	SetBkMode(hdc, TRANSPARENT); //set background
	SelectObject(hdc, hFontInside);
	SetTextColor(hdc, RGB(255, 255, 255));
	
	for (int i = 0; i < this->listTags.size(); ++i) //phần chú thích là các hình vuông
	{
		if (listTags[i].amount>0)
		{
			SetDCBrushColor(hdc, RGB(listTags[i].color.r, listTags[i].color.g, listTags[i].color.b));
			Rectangle(hdc, CorOfPie[i].x, CorOfPie[i].y, CorOfPie[i].x + 40, CorOfPie[i].y + 40);
			RECT rect = { CorOfPie[i].x, CorOfPie[i].y, CorOfPie[i].x + 40, CorOfPie[i].y + 40 };
			DrawText(hdc, (to_wstring((int)round(this->GetSweepAngle(i) / 360 * 100)) + L'%').c_str(), -1, &rect, DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
		}
	}

	SelectObject(hdc, hFontOutside);
	SetTextColor(hdc, RGB(0, 0, 0));
	for (int i = 0; i < this->listTags.size(); ++i)
	{
		if (listTags[i].amount>0)
		{
			RECT rect = { CorOfPie[i].x + 40, CorOfPie[i].y, CorOfPie[i].x + 100, CorOfPie[i].y + 50 };
			DrawText(hdc, listTags[i].name.c_str(), -1, &rect, DT_SINGLELINE | DT_NOCLIP | DT_LEFT | DT_VCENTER);
		}
	}
}
Chart::~Chart()
{
}
