//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by QuickNote.rc
//
#define IDC_MYICON                      2
#define IDD_QUICKNOTE_DIALOG            102
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_QUICKNOTE                   107
#define IDI_SMALL                       108
#define IDC_QUICKNOTE                   109
#define IDS_APPTOOLTIP                  112
#define IDM_VIEWNOTE                    113
#define IDM_VIEWSTATISTICS              114
#define ID_TAG                          121
#define ID_SAVE                         122
#define IDC_CBBOX_TAG                   123
#define IDC_LISTBOX_TAG                 124
#define IDC_BTN_NEW_NOTE                127
#define IDR_MAINFRAME                   128
#define IDC_EDIT_NOTE                   128
#define IDC_EDIT_TAG                    129
#define IDD_NOTE                        130
#define IDD_VIEWNOTE                    130
#define IDD_STATISTIC                   131
#define IDC_LISTNOTE                    131
#define IDI_ICON1                       133
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           132
#endif
#endif
