﻿#pragma once
#include <vector>
#include "TAG.h"
#include "NOTE.h"

/*****************Lớp đồ thị*********************/
struct Coord { int x, y; };

//màu sắc
struct RGBColor { int r, g, b; };

struct TagInChart
{
	wstring name;
	int amount;
	RGBColor color;
	float xStartAngle;//điểm bắt đầu của 1 cung
	float xSweepAngle; //độ lớn của 1 cung
};

class Chart
{
private:
	Coord origin; //tâm
	int radius; //bán kính
	int total;
	vector<TagInChart> listTags; //danh sách các loại Tag
	float GetSweepAngle(int);
public:
	Chart();
	Chart(Coord, int);
	void Draw(HDC hdc); //vẽ chart
	void UpdateCoord(vector<TAG>); //cập nhật lại tọa độ
	void ShowNote(HDC hdc); //chú thích
	int GetTotal(vector<TAG>);
	~Chart();
};