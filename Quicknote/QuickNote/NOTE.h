#pragma once
#include "stdafx.h"
#include <vector>
#include <string>

using namespace std;

class NOTE
{
public:
	long Id;
	vector<int> vTags; //list of tag
	wstring Content;
	FILETIME time;
public:

	NOTE()
	{
	}
	NOTE(long id, vector<int> tag, wstring content)
	{
		Id = id;
		vTags = tag;
		Content = content;
	}
	NOTE(long id, vector<int> tag, wstring content, FILETIME t)
	{
		Id = id;
		vTags = tag;
		Content = content;
		time = t;
	}
	~NOTE()
	{
	}
};

