﻿// QuickNote.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "QuickNote.h"
#include "Shellapi.h"

#define MAX_LOADSTRING 100
#define	WM_USER_SHELLICON WM_USER + 1

// Global Variables:
HINSTANCE hInst;			// current instance
NOTIFYICONDATA nidApp;
HMENU hPopMenu;
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];	// the main window class name
TCHAR szApplicationToolTip[MAX_LOADSTRING];
BOOL bDisable = FALSE;							// keep application state

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_QUICKNOTE, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_QUICKNOTE));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_QUICKNOTE);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON1));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;
   HICON hMainIcon;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
	   CW_USEDEFAULT, 0, 500, 600, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   hMainIcon = LoadIcon(hInstance, (LPCTSTR)MAKEINTRESOURCE(IDI_ICON1));

   nidApp.cbSize = sizeof(NOTIFYICONDATA); // sizeof the struct in bytes 
   nidApp.hWnd = (HWND)hWnd;              //handle of the window which will process this app. messages 
   nidApp.uID = IDI_ICON1;           //ID of the icon that willl appear in the system tray 
   nidApp.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP; //ORing of all the flags 
   nidApp.hIcon = hMainIcon; // handle of the Icon to be displayed, obtained from LoadIcon 
   nidApp.uCallbackMessage = WM_USER_SHELLICON;
   LoadString(hInstance, IDS_APPTOOLTIP, nidApp.szTip, MAX_LOADSTRING);
   Shell_NotifyIcon(NIM_ADD, &nidApp);
   //ShowWindow(hWnd, nCmdShow); //Ẩn màn hình chính
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (updateTagStr)
	{
		SendMessage(hWndCBBTag, WM_SETTEXT, -1, (LPARAM)edtTagStr.c_str());
		updateTagStr = false;
	}

	switch (message)
	{
	case WM_USER_SHELLICON:
		// systray msg callback 
		switch (LOWORD(lParam))
		{
		case WM_RBUTTONDOWN:
			POINT  lpClickPoint;
			UINT uFlag = MF_BYPOSITION | MF_STRING;
			GetCursorPos(&lpClickPoint);
			hPopMenu = CreatePopupMenu();
			InsertMenu(hPopMenu, 0xFFFFFFFF, uFlag, IDM_VIEWNOTE, _T("View notes")); // notes
			InsertMenu(hPopMenu, 0xFFFFFFFF, uFlag, IDM_VIEWSTATISTICS, _T("View statistics")); // statistics		
			InsertMenu(hPopMenu, 0xFFFFFFFF, MF_BYPOSITION | MF_STRING, IDM_EXIT, L"Exit");

			SetForegroundWindow(hWnd);
			TrackPopupMenu(hPopMenu, TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_BOTTOMALIGN, lpClickPoint.x, lpClickPoint.y, 0, hWnd, NULL);
			return TRUE;

		}
		break;
	case WM_CREATE:
	{
					  doInstallHook(hWnd);
					  RECT rect;
					  static HDC hdc;
					  if (GetWindowRect(hWnd, &rect))
					  {
						  Manager = new MANAGER();

						  //BTN
						  hCurNote = CreateWindow(L"Static", L"Current Note:", WS_CHILD | WS_VISIBLE, 20, 20, 450, 30, hWnd, NULL, hInst, NULL);
						  hTag = CreateWindow(L"Static", L"Tag:", WS_CHILD | WS_VISIBLE, 20, 370, 450, 30, hWnd, NULL, hInst, NULL);

						  hWndEdtNote = CreateWindowEx(WS_EX_CLIENTEDGE, L"Edit", L"", WS_CHILD | WS_VISIBLE | ES_AUTOHSCROLL | WS_VSCROLL | WS_HSCROLL | ES_AUTOVSCROLL | ES_MULTILINE,
							  20, 70, 450, 250, hWnd, (HMENU)IDC_EDIT_NOTE, hInst, NULL);
						  //hWndCBBTag = CreateWindowEx(WS_EX_CLIENTEDGE, L"Edit", L"", WS_CHILD | WS_VISIBLE, 20, 410, 450, 25, hWnd, (HMENU)IDC_EDIT_TAG, hInst, NULL);
						  hWndCBBTag = TagOfANote(hWnd, hInst, IDC_CBBOX_TAG);
						  hWndBtnSave = CreateWindowEx(0, L"Button", L"Save", WS_CHILD | WS_VISIBLE, 
							  350, 500, 90, 25, hWnd, (HMENU)ID_SAVE, hInst, NULL);

						  HFONT hFont = CreateFont(23, 0, 0, 0, 700, TRUE, FALSE, FALSE, ANSI_CHARSET,
							  OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
							  DEFAULT_PITCH | FF_DONTCARE, TEXT("Calibri"));

						  SendMessage(hCurNote, WM_SETFONT, (WPARAM)hFont, TRUE);
						  SendMessage(hTag, WM_SETFONT, (WPARAM)hFont, TRUE);

						  hFont = CreateFont(18, 0, 0, 0, 500, FALSE, FALSE, FALSE, ANSI_CHARSET,
							  OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
							  DEFAULT_PITCH | FF_DONTCARE, TEXT("Times New Roman"));

						  SendMessage(hWndEdtNote, WM_SETFONT, (WPARAM)hFont, TRUE);
						  SendMessage(hWndCBBTag, WM_SETFONT, (WPARAM)hFont, TRUE);

						  SendMessage(hWndCBBTag, CB_RESETCONTENT, 0, 0);
						  Manager->loadFromFile();
						  updateCombox(Manager, hWndCBBTag);

						  return 0;
					  }
					  break;
	}

	case WM_COMMAND:
	{
					   int wmId = LOWORD(wParam);
					   // Parse the menu selections:
					   switch (wmId)
					   {
					   case IDC_CBBOX_TAG:
					   {

											 if (HIWORD(wParam) == CBN_SELCHANGE)
											 {
												 SendMessage(hWndCBBTag, EM_SETSEL, 0, -1);
												 int ItemIndex = SendMessage((HWND)lParam, (UINT)CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
												 TCHAR buff[1024];
												
												 LRESULT result = SendMessage(hWndCBBTag, WM_GETTEXT, sizeof(buff), (LPARAM)buff);
												 edtTagStr = buff;

												 SendMessage((HWND)lParam, (UINT)CB_GETLBTEXT, (WPARAM)ItemIndex, (LPARAM)buff);
												 if (result != 0)
												 {
													 edtTagStr += L", ";
												 }
												 
												 edtTagStr += buff;

												 SendMessage(hWndCBBTag, WM_SETTEXT, 0, (LPARAM)edtTagStr.c_str());
												 updateTagStr = true;
											 }

					   }
						   break;
					   case IDC_EDIT_NOTE://your edit control ID
					   {
											  if (HIWORD(wParam) == EN_CHANGE) //notification)   
											  {
												  InvalidateRect(hWndEdtNote, NULL, TRUE);
											  }
											  break;
					   }
					   case ID_SAVE:
					   {
									   vector<wstring> vError;
									   wstring noteData = GetTextboxWindow(hWndEdtNote);
									   wstring tagData = GetTextboxWindow(hWndCBBTag);

									   if (noteData.length() == 0) vError.push_back(L"Nội dung note!");
									   if (tagData.length() == 0) vError.push_back(L"Tag!");

									   if (vError.size() > 0)
									   {
										   wstring wShowError;
										   for (int i = 0; i < vError.size(); ++i) wShowError += vError[i] + L'\n';
										   MessageBox(hWnd, wShowError.c_str(), L"Bạn chưa điền", MB_OK | MB_ICONERROR);
									   }
									   else
									   {
										   Manager->saveNoteToList(tagData, noteData);
										   int err = Manager->saveToFile();

										   if (err != 0)
										   {
											   wstringstream ss;
											   ss << L"Can't open file!\n Err code = " << err;
											   MessageBox(hWnd, ss.str().c_str(), L"Notification", MB_OK);
										   }
										   else
										   {
											   MessageBox(hWnd, L"Saved!", L"Notification", MB_OK);
											   SendMessage(hWndEdtNote, WM_SETTEXT, 0, (LPARAM)L"");
											   SendMessage(hWndCBBTag, WM_SETTEXT, 0, (LPARAM)L"");
											   updateCombox(Manager, hWndCBBTag);
											   edtTagStr = L"";
										   }
									   }
									   break;
					   }
					   case IDM_VIEWNOTE:
					   {
											ShowWindow(hWnd, SW_HIDE);
											DialogBox(hInst, MAKEINTRESOURCE(IDD_NOTE), hWnd, ViewNotesWndProc);
											break;
					   }
					   case IDM_VIEWSTATISTICS:
					   {
												  ShowWindow(hWnd, SW_HIDE);
												  DialogBox(hInst, MAKEINTRESOURCE(IDD_STATISTIC), hWnd, ViewStatisticsWndProc);
					   }
						   break;
					   case IDM_EXIT:
						   doRemoveHook(hWnd);
						   DestroyWindow(hWnd);
						   break;
					   default:
						   return DefWindowProc(hWnd, message, wParam, lParam);
					   }
	}
		break;
	case WM_CLOSE:
	{
						ShowWindow(hWnd, SW_HIDE); //hWnd is the handle of the application window
						return TRUE;
	}
	break;
	case WM_DESTROY:
		doRemoveHook(hWnd);
		Shell_NotifyIcon(NIM_DELETE, &nidApp);
		PostQuitMessage(0);
		break;
	case WM_CTLCOLORSTATIC:
	{
							  HDC hdc = (HDC)wParam;
							  SetBkMode(hdc, TRANSPARENT);

							  HBRUSH backgroundColor = CreateSolidBrush(RGB(0, 128, 64));
							  return (LRESULT)backgroundColor; // or any other brush you want
	}
		break;
	case WM_CTLCOLORLISTBOX:
	{
							   if ((HWND)lParam == hWndNotesList || (HWND)lParam == hWndTagsList) {
								   HDC hdc = (HDC)wParam;
								   SetBkMode(hdc, TRANSPARENT);

								   HBRUSH backgroundColor = CreateSolidBrush(RGB(255, 255, 255));
								   return (LRESULT)backgroundColor; // or any other brush you want
							   }
							   else
							   {
								   return DefWindowProc(hWnd, message, wParam, lParam);
							   }
							   break;
	}
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

INT_PTR CALLBACK ViewNotesWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{

	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_PAINT:
	{
					 PAINTSTRUCT ps;
					 HDC hdc = BeginPaint(hDlg, &ps);
					 HFONT hFont_Title = CreateFont(30, 0, 0, 0, 1550, FALSE, FALSE, FALSE, ANSI_CHARSET,
						 OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
						 DEFAULT_PITCH | FF_DONTCARE, TEXT("Courier New"));

					 HWND hWndStatic = CreateWindow(L"Static", L"VIEW NOTES", WS_CHILD | WS_VISIBLE | SS_CENTER, 100, 30, 300, 30, hDlg, NULL, hInst, NULL);
					 SendMessage(hWndStatic, WM_SETFONT, (WPARAM)hFont_Title, TRUE);

					 hTagList = CreateWindow(L"Static", L"List Of Tags:", WS_CHILD | WS_VISIBLE, 10, 70, 150, 20, hDlg, NULL, hInst, NULL);
					 hNoteList = CreateWindow(L"Static", L"Notes Of A Tag:", WS_CHILD | WS_VISIBLE, 220, 70, 150, 20, hDlg, NULL, hInst, NULL);
					 hContentNote = CreateWindow(L"Static", L"Content:", WS_CHILD | WS_VISIBLE, 10, 440, 150, 20, hDlg, NULL, hInst, NULL);
					 
					 hWndTagsList = CreateWindowEx(WS_EX_CLIENTEDGE, L"ListBox", L"", WS_CHILD | WS_VISIBLE | WS_VSCROLL | LBS_NOTIFY | LBS_SORT, 10, 100, 200, 320, hDlg, (HMENU)IDC_LISTBOX_TAG, hInst, NULL);
					 hWndNotesList = ListNotesOfATag(hDlg, hInst, IDC_LISTNOTE);
					 hWndContNote = CreateWindow(L"Edit", L"", WS_CHILD | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL | ES_MULTILINE, 10, 470, 550, 150, hDlg, NULL, hInst, NULL);

					  HFONT hFont = CreateFont(23, 0, 0, 0, 700, TRUE, FALSE, FALSE, ANSI_CHARSET,
						  OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
						  DEFAULT_PITCH | FF_DONTCARE, TEXT("Calibri"));

					  SendMessage(hTagList, WM_SETFONT, (WPARAM)hFont, TRUE);
					  SendMessage(hNoteList, WM_SETFONT, (WPARAM)hFont, TRUE);
					  SendMessage(hContentNote, WM_SETFONT, (WPARAM)hFont, TRUE);

					  hFont = CreateFont(18, 0, 0, 0, 500, FALSE, FALSE, FALSE, ANSI_CHARSET,
						  OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
						  DEFAULT_PITCH | FF_DONTCARE, TEXT("Times New Roman"));

					  SendMessage(hWndTagsList, WM_SETFONT, (WPARAM)hFont, TRUE);
					  SendMessage(hWndNotesList, WM_SETFONT, (WPARAM)hFont, TRUE);
					  SendMessage(hWndContNote, WM_SETFONT, (WPARAM)hFont, TRUE);

					  Manager->loadFromFile();

					  updateLBTag(Manager, hWndTagsList);

					  EndPaint(hDlg, &ps);

					  return 0;
	}
		break;
		static int tagPos;
	case WM_COMMAND:
	{
					   int wmId = LOWORD(wParam);
					   // Parse the menu selections:
					   switch (wmId)
					   {
					   case IDC_LISTBOX_TAG:
					   {
											   switch (HIWORD(wParam))
											   {
											   case LBN_SELCHANGE:
											   {
																	 tagPos = getSltedTagPos(Manager, hWndTagsList);
																	 updateViewNotes(Manager, hWndNotesList, tagPos);
																	 break;
											   }
											   }
											   break;
					   }

						   break;
					   }
	}
		break;
	case WM_NOTIFY:
	{
					  NMHDR* notifyMess = (NMHDR*)lParam;
					  switch (notifyMess->code)
					  {
					  case NM_CLICK:
					  {
									   if (notifyMess->hwndFrom == hWndNotesList)
									   {
										   int SltedNoteID = getSltedNoteID(Manager, hWndNotesList, tagPos);
										   static NOTE note = Manager->NoteList[SltedNoteID];
										   updateContNote(hWndContNote, Manager->NoteList[SltedNoteID]);
									   }
					  }
						  break;
					  }

	}
		break;
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_CLOSE:
		EndDialog(hDlg, LOWORD(wParam));
		return (INT_PTR)TRUE;
		break;
	}
	return (INT_PTR)FALSE;
}

//Statistic diaglog
INT_PTR CALLBACK ViewStatisticsWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_PAINT:
	{
					 PAINTSTRUCT ps;
					 HDC hdc = BeginPaint(hDlg, &ps);
					 HFONT hFont_Title = CreateFont(30, 0, 0, 0, 1550, FALSE, FALSE, FALSE, ANSI_CHARSET,
						 OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
						 DEFAULT_PITCH | FF_DONTCARE, TEXT("Courier New"));

					 HWND hWndStatic = CreateWindow(L"Static", L"TOP 5 USED TAGS", WS_CHILD | WS_VISIBLE | SS_CENTER, 100, 30, 300, 30, hDlg, NULL, hInst, NULL);

					 SendMessage(hWndStatic, WM_SETFONT, (WPARAM)hFont_Title, TRUE);

					 HFONT hFont = CreateFont(16, 0, 0, 0, 550, FALSE, FALSE, FALSE, ANSI_CHARSET,
						 OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
						 DEFAULT_PITCH | FF_DONTCARE, TEXT("Courier New"));

					 Manager->loadFromFile();
					 //sort
					 vector<TAG> tagList = Manager->TagList;
					 sort(tagList.begin(), tagList.end(), sortTagList);

					 int radius = 100;
					 Coord origin = { 250, radius + 100 };
					 mChart = new Chart(origin, radius);
					 mChart->UpdateCoord(tagList);
					 mChart->Draw(hdc);
					 mChart->ShowNote(hdc);
					 EndPaint(hDlg, &ps);
					 break;
	}
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;
		break;
	case WM_CLOSE:
		EndDialog(hDlg, LOWORD(wParam));
		return (INT_PTR)TRUE;
		break;
	}
	return (INT_PTR)FALSE;
}

int updateLBTag(MANAGER *Manager, HWND hWndLB)
{
	SendMessage(hWndLB, LB_RESETCONTENT, NULL, NULL);
	wstringstream ss;
	for (int i = 0; i < Manager->TagList.size(); i++)
	{
		wstringstream().swap(ss);
		ss << Manager->TagList[i].TagName << "(" << Manager->TagList[i].Id.size() << ")";

		SendMessage(hWndLB, LB_ADDSTRING, 0, (LPARAM)ss.str().c_str());
	}
	
	return 0;
}
int updateViewNotes(MANAGER *Manager, HWND hWndLB, int tagPos)
{
	ListView_DeleteAllItems(hWndLB);
	wstring wsNotePreview;
	int noteId;
	for (int i = 0; i < Manager->TagList[tagPos].Id.size(); i++)
	{
		LV_ITEM lv;
		noteId = Manager->TagList[tagPos].Id[i];
		//Nạp cột đầu tiên loại chi tiêu
		lv.mask = LVIF_TEXT | LVIF_PARAM;
		lv.iItem = i;

		lv.iSubItem = 0;
		lv.pszText = new WCHAR[54];
		wsNotePreview = Manager->NoteList[noteId].Content.substr(0, 50);
		wsNotePreview += L"...";
		wcscpy(lv.pszText, wsNotePreview.c_str());
		ListView_InsertItem(hWndLB, &lv);

		ListView_SetItemText(hWndLB, i, 1, convertTimeStampToString(Manager->NoteList[noteId].time));
	}
	return 0;
}
int updateContNote(HWND hWndEdtNote, NOTE note)
{
	SendMessage(hWndEdtNote, WM_SETTEXT, 0, (LPARAM)note.Content.c_str());

	return 0;
}
int getSltedTagPos(MANAGER *Manager, HWND hWndTagList)
{
	// Get selected index.
	int itemIndex = (int)SendMessage(hWndTagList, LB_GETCURSEL, (WPARAM)0, (LPARAM)0);
	if (itemIndex == LB_ERR)// No selection
	{
		return NULL;
	}
	WCHAR selectedTagStr[100];

	// Get item data.
	int i = (int)SendMessage(hWndTagList, LB_GETTEXT, itemIndex, (LPARAM)selectedTagStr);

	wstring wsSelectedTagStr = selectedTagStr;

	while (wsSelectedTagStr[wsSelectedTagStr.size() - 1] != '(')
	{
		wsSelectedTagStr.pop_back();
	}
	wsSelectedTagStr.pop_back();

	//find match tag in tags list
	int Pos = -1;
	for (int i = 0; i < Manager->TagList.size(); i++)
	{
		if (Manager->TagList[i].TagName == wsSelectedTagStr)
		{
			Pos = i;
			break;
		}
	}

	return Pos;
}
int getSltedNoteID(MANAGER *Manager, HWND hWndNotesList, int tagPos)
{
	int nCurSelIndex = SendMessage(hWndNotesList, LVM_GETNEXTITEM, -1, LVNI_FOCUSED);
	return Manager->TagList[tagPos].Id[nCurSelIndex];
}

bool sortTagList(TAG tag1, TAG tag2) { return tag1.Id.size() > tag2.Id.size(); }

void doInstallHook(HWND hWnd)
{
	typedef VOID(*MYPROC)(HWND);

	HINSTANCE hinstLib;
	MYPROC ProcAddr;

	// load DLL và lấy handle của DLL module
	hinstLib = LoadLibrary(L"DLLHook.dll");
	// Nếu load thành công, lấy địa chỉ của hàm DrawCircle trong DLL
	if (hinstLib != NULL) {
		ProcAddr = (MYPROC)GetProcAddress(hinstLib, "_doInstallHook");
		// Nếu lấy được địa chỉ hàm, gọi thực hiện hàm
		if (ProcAddr != NULL)
			ProcAddr(hWnd);
		else MessageBox(hWnd, L"", 0, 0);
	}
}

void doRemoveHook(HWND hWnd)
{
	typedef VOID(*MYPROC)(HWND);

	HINSTANCE hinstLib;
	MYPROC ProcAddr;

	// load DLL và lấy handle của DLL module
	hinstLib = LoadLibrary(L"DLLHook.dll");
	// Nếu load thành công, lấy địa chỉ của hàm DrawCircle trong DLL
	if (hinstLib != NULL) {
		ProcAddr = (MYPROC)GetProcAddress(hinstLib, "_doRemoveHook");
		// Nếu lấy được địa chỉ hàm, gọi thực hiện hàm
		if (ProcAddr != NULL)
			ProcAddr(hWnd);
		else MessageBox(hWnd, 0, 0, 0);
	}
}

wstring GetTextboxWindow(HWND hwnd)
{
	int bufferSize = GetWindowTextLength(hwnd);
	WCHAR* buffer = new WCHAR[bufferSize + 1];

	GetWindowText(hwnd, buffer, bufferSize + 1);
	wstring result(buffer);
	delete[] buffer;
	return result;
}

HWND ListNotesOfATag(HWND hDlg, HINSTANCE hInst, long ID)
{
	InitCommonControls();
	HWND NotesList = CreateWindowEx(LVS_EX_FULLROWSELECT, WC_LISTVIEW, L"",
		WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL | LVS_REPORT | LVS_EX_GRIDLINES, 220, 100, 450, 300, hDlg, (HMENU)ID, hInst, NULL);
	SendMessage(NotesList, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_FULLROWSELECT);

	LVCOLUMN lvCol;

	// Date column
	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol.cx = 300;
	lvCol.pszText = _T("Quotes");
	ListView_InsertColumn(NotesList, 0, &lvCol);

	// quote column
	lvCol.fmt = LVCFMT_LEFT;
	lvCol.cx = 150;
	lvCol.pszText = _T("Date Modified");
	ListView_InsertColumn(NotesList, 1, &lvCol);
	return NotesList;
}

HWND TagOfANote(HWND hDlg, HINSTANCE hInst, long ID)
{
	HWND hwndtag = CreateWindowEx(WS_EX_STATICEDGE, L"COMBOBOX", L"MyCombo1",
		CBS_DROPDOWN | WS_VSCROLL | WS_OVERLAPPED | WS_CHILD | WS_VISIBLE,
		20, 410, 450, 25, hDlg, (HMENU)ID, hInst, NULL);

	//sort
	vector<TAG> tagList = Manager->TagList;
	sort(tagList.begin(), tagList.end(), sortTagList);

	for (int i = 0; i < tagList.size(); i++) //Add string to combobox.
	{
		SendMessage(hwndtag, CB_ADDSTRING,
			0, LPARAM(tagList.at(i).TagName.c_str()));
	}

	return hwndtag;
}

int updateCombox(MANAGER *Manager, HWND hWndCB)
{
	SendMessage(hWndCB, CB_RESETCONTENT, 0, 0);
	//sort
	vector<TAG> tagList = Manager->TagList;
	sort(tagList.begin(), tagList.end(), sortTagList);

	//add to dropdown list
	
	for (int i = 0; i < tagList.size(); i++)
	{
		SendMessage(hWndCBBTag, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)tagList[i].TagName.c_str());
	}

	return 0;
}
LPWSTR convertTimeStampToString(const FILETIME &ftLastWrite)
{
	TCHAR *buffer = new TCHAR[50];

	SYSTEMTIME st;

	char szLocalDate[255], szLocalTime[255];

	FileTimeToSystemTime(&ftLastWrite, &st);
	GetDateFormat(LOCALE_USER_DEFAULT, DATE_AUTOLAYOUT, &st, NULL,
		(LPWSTR)szLocalDate, 255);
	GetTimeFormat(LOCALE_USER_DEFAULT, 0, &st, NULL, (LPWSTR)szLocalTime, 255);

	//Concat to string
	wsprintf(buffer, L"%s %s", szLocalDate, szLocalTime);

	return buffer;
}