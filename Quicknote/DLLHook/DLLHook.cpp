﻿// DLLHook.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <Windowsx.h>

#if defined(_WIN32)
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

HWND hWnd;
HHOOK hHook = NULL;
HINSTANCE hinstLib;
int code = -1;

LRESULT CALLBACK MouseHookProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode < 0) // không xử lý message 
		return CallNextHookEx(hHook, nCode, wParam, lParam);
	// xử lý message: Windows + Space mouse click
	MOUSEHOOKSTRUCT *mHookData = (MOUSEHOOKSTRUCT *)lParam;
	if (GetAsyncKeyState(VK_SPACE) != 0 && GetAsyncKeyState(VK_LWIN) != 0)
	{
		if (IsIconic(hWnd))
		{
			ShowWindow(hWnd, SW_SHOW);
		}
		else
		{
			ShowWindow(hWnd, SW_MINIMIZE);
		}
	}

	return CallNextHookEx(hHook, nCode, wParam, lParam);
}

extern "C" EXPORT void _doInstallHook(HWND hWnd);
void _doInstallHook(HWND hwndYourWindow)
{
	hWnd = hwndYourWindow;
	if (hHook != NULL) return;

	hHook = SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC)MouseHookProc, hinstLib, 0);
}

extern "C" EXPORT void _doRemoveHook(HWND hWnd);
void _doRemoveHook(HWND hWnd)
{
	if (hHook == NULL) return;
	UnhookWindowsHookEx(hHook);
	hHook = NULL;
}
