﻿#include "stdafx.h"
#include "Chart.h"


Chart::Chart()
{
}

Chart::Chart(Coord co, int r)
{
	this->origin.x = co.x;
	this->origin.y = co.y;
	this->radius = r;

	this->listType.resize(6);
	this->listType[0].color = { 231, 76, 60 };
	this->listType[1].color = { 52, 152, 219 };
	this->listType[2].color = { 39, 174, 96 };
	this->listType[3].color = { 142, 68, 173 };
	this->listType[4].color = { 211, 84, 0 };
	this->listType[5].color = { 44, 62, 80 };
}

float Chart::GetSweepAngle(int index) 
{
	int total = 0;
	for (int i = 0; i < this->listType.size(); ++i)
	{
		total += this->listType[i].amount;
	}

	float percent = (float)this->listType[index].amount / total;
	return float(percent *360);
}

void Chart::Draw(HDC hdc)
{
	for (int i = 0; i < this->listType.size(); ++i)
	{
		if (this->listType[i].amount!= 0)
		{
			SetDCBrushColor(hdc, RGB(this->listType[i].color.r, this->listType[i].color.g, this->listType[i].color.b));
			SetDCPenColor(hdc, RGB(241, 241, 241));
			SelectObject(hdc, GetStockObject(DC_BRUSH));
			SelectObject(hdc, GetStockObject(DC_PEN));
			//tham khảo drawing a pie chart trên msdn
			BeginPath(hdc);
			MoveToEx(hdc, this->origin.x, this->origin.y, NULL);
			AngleArc(hdc, this->origin.x, this->origin.y, this->radius, this->listType[i].xStartAngle, this->listType[i].xSweepAngle);
			LineTo(hdc, this->origin.x, this->origin.y);
			EndPath(hdc);
			StrokeAndFillPath(hdc); //vẽ viền và tô màu bằng màu hiện tại cho cung tròn
		}
	}
}

void Chart::UpdateCoord(vector<SpendingType>listType)
{
	for (int i = 0; i < this->listType.size(); ++i)
	{
		this->listType[i].name = listType[i].name;
		this->listType[i].amount = listType[i].totalCost;
	}

	for (int i = 0; i < this->listType.size(); ++i)
	{
		this->listType[i].xSweepAngle = this->GetSweepAngle(i);
	}

	this->listType[0].xStartAngle = 90;

	for (int i = 1; i < this->listType.size(); ++i)
	{
		this->listType[i].xStartAngle = this->listType[i - 1].xStartAngle + this->listType[i - 1].xSweepAngle;
	}
}
void Chart::ShowNote(HDC hdc)
{
//tham khảo CreateFont function msdn
	
	HFONT hFontInside = CreateFont(24, 0, 0, 0, 600, FALSE, FALSE, FALSE, VIETNAMESE_CHARSET, 
		OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Calibri")); 
	HFONT hFontOutside = CreateFont(25, 0, 0, 600, FW_DONTCARE, FALSE, FALSE, FALSE, VIETNAMESE_CHARSET,
		OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Calibri"));
	vector<Coord> CorOfPie = {
		{ 660, 470 },
		{ 800, 470 },
		{ 1000, 470 },
		{ 660, 540 },
		{ 800, 540 },
		{ 1000, 540 },
	}; 
	SetBkMode(hdc, TRANSPARENT); //set background
	SelectObject(hdc, hFontInside);
	SetTextColor(hdc, RGB(255, 255, 255));

	for (int i = 0; i < this->listType.size(); ++i) //phần chú thích là các hình vuông
	{
		SetDCBrushColor(hdc, RGB(listType[i].color.r, listType[i].color.g, listType[i].color.b));
		Rectangle(hdc, CorOfPie[i].x, CorOfPie[i].y, CorOfPie[i].x + 40, CorOfPie[i].y + 40);
		RECT rect = { CorOfPie[i].x, CorOfPie[i].y, CorOfPie[i].x + 40, CorOfPie[i].y + 40 };
		DrawText(hdc, (to_wstring((int)round(this->GetSweepAngle(i) / 360 * 100)) + L'%').c_str(), -1, &rect, DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
	}
	
	SelectObject(hdc, hFontOutside);
	SetTextColor(hdc, RGB(0, 0, 0));
	for (int i = 0; i < this->listType.size(); ++i)
	{
		RECT rect = { CorOfPie[i].x + 50, CorOfPie[i].y, CorOfPie[i].x + 100, CorOfPie[i].y + 50 };
		DrawText(hdc, listType[i].name.c_str(), -1, &rect, DT_SINGLELINE | DT_NOCLIP | DT_LEFT | DT_VCENTER);
	}
}
Chart::~Chart()
{
}
