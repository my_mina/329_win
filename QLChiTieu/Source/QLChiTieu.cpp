﻿// QLChiTieu.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "QLChiTieu.h"
#include <windowsx.h>
#include <CommCtrl.h>
#include "GetText.h"

#pragma comment(lib, "comctl32.lib")

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_QLCHITIEU, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_QLCHITIEU));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_QLCHITIEU));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_QLCHITIEU);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   m_hWnd = CreateWindow(szWindowClass, L"Quản lý chi tiêu", WS_OVERLAPPEDWINDOW| WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
	   10, 10, 1200, 680, NULL, NULL, hInstance, NULL);

   if (!m_hWnd)
   {
      return FALSE;
   }

   ShowWindow(m_hWnd, nCmdShow);
   UpdateWindow(m_hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;

	switch (message)
	{
	case WM_CREATE:
	{
					  mFile = new FileStream("data.txt"); //đọc danh sách chi tiêu từ tập tin
					  mHandle = new Handles(mFile->ReadFile()); //khởi tạo các handle
					  hFont = CreateFont(16, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, VIETNAMESE_CHARSET,
						  OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Calibri"));

					  IncludeAddSpendingControls(hWnd); //tạo group box thêm chi tiêu
					  IncludeListSpendingControls(hWnd); //tạo group box list view hiển thị danh sách chi tiêu và tổng chi tiêu
					  IncludePieChartControls(hWnd); //tạo biểu đồ
	}
		break;
	case WM_DESTROY:
		mFile->SaveFile(mHandle->GetSpendingList());
		PostQuitMessage(0);
		break;
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			mFile->SaveFile(mHandle->GetSpendingList());
			DestroyWindow(hWnd);
			break;
		}
		break;
	case WM_PAINT:
	{

					 PAINTSTRUCT ps;

					 hdc = BeginPaint(hWnd, &ps);

					 mChart->Draw(hdc);
					 mChart->ShowNote(hdc);

					 EndPaint(hWnd, &ps);
					 break;
	}
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

LRESULT CALLBACK GBSpendingWndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{ 
	case WM_COMMAND:
	{
					   wmId = LOWORD(wParam);
					   switch (wmId)
					   {
					   case IDC_MYADDBUTTON:
					   {
											   LRESULT wSelection = SendMessage(hSpendingType, CB_GETCURSEL, 0, 0);
											   vector<wstring> vError;
											   Spending spd;
											   wstring info = GetText::GetDetailText(hDetail);
											   int amount = GetText::GetCostText(hCost);

											   if (wSelection < 0) vError.push_back(L"Lỗi #001: Bạn phải chọn một loại chi tiêu!");
											   if (info.length() == 0) vError.push_back(L"Lỗi #002: Bạn phải nhập chi tiết loại chi tiêu!");

											   if (amount == INT_MIN) vError.push_back(L"Lỗi #003: Bạn phải nhập số tiền!");
											   else if (amount <= 0) vError.push_back(L"Lỗi #004: Số tiền đã nhập không hợp lệ!");

											   if (vError.size() > 0)
											   {
												   wstring wShowError;
												   for (int i = 0; i < vError.size(); ++i) wShowError += vError[i] + L'\n';
												   MessageBox(hwnd, wShowError.c_str(), L"Lỗi", MB_OK | MB_ICONERROR);
											   }
											   else
											   {
												   spd.SetSpdType(GetText::GetComboboxText(hSpendingType, wSelection));
												   spd.SetSpdDetail(info);
												   spd.SetSpdCost(amount);
												   
												   mHandle->AddSpendingItem(spd);
												   mChart->UpdateCoord(mHandle->GetListSpendingType());

												   InvalidateRect(m_hWnd, NULL, TRUE);
												   ClearAllFields();
												   
											   }
											   break;
					   }
					   }
	}
		break;	
	}
	
	return CallWindowProc(wndProcSpeding, hwnd, message, wParam, lParam);
}

LRESULT CALLBACK GBListviewWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_NOTIFY:
	{
					  NMHDR* pnm = (NMHDR*)lParam;
					  switch (pnm->code)
					  {
					  case NM_RCLICK:
					  {
										lvSelectID = SendMessage(lvSpending, LVM_GETNEXTITEM, -1, LVNI_SELECTED);

										if (lvSelectID != -1)
										{
											SendMessage(lvSpending, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_FULLROWSELECT);
											hPopupMenu = CreatePopupMenu();
											//InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING, IDM_LVEDIT, L"Sửa");
											InsertMenu(hPopupMenu, 1, MF_BYPOSITION | MF_STRING, IDM_LVDELETE, L"Xoá");
											POINT p;
											GetCursorPos(&p);
											TrackPopupMenu(hPopupMenu, TPM_TOPALIGN | TPM_LEFTALIGN, p.x, p.y, 0, hWnd, NULL);
										}
					  }
						  break;
					  }
	}
					  break;
	case WM_COMMAND:
	{
					   int wmId = LOWORD(wParam);
					   switch (wmId)
					   {
					   case IDM_LVDELETE:
					   {
											int result = MessageBox(0, L"Bạn có chắc chắn muốn xoá?", L"Thông báo", MB_OKCANCEL | MB_ICONQUESTION);

											if (result == 1)
											{
												mHandle->DeleteSpendingItem(lvSelectID);
												mChart->UpdateCoord(mHandle->GetListSpendingType());

												InvalidateRect(m_hWnd, NULL, TRUE);
											}

					   }
						   break;
					   }
	}
		break;
	}
	return CallWindowProc(wndProcListview, hWnd, message, wParam, lParam);
}

void IncludeAddSpendingControls(HWND hWnd)
{
	gbSpending = CreateWindowEx(0, L"Button", L"Thêm chi tiêu", WS_CHILD | WS_VISIBLE | BS_GROUPBOX,
		10, 10, 600, 140, hWnd, NULL, hInst, NULL);
	SendMessage(gbSpending, WM_SETFONT, WPARAM(hFont), TRUE); //group box thêm chi tiêu

	HandleProperties prop = { 30, 50, 150, 300, NULL };
	hSpendingType = mHandle->GetHandleSpendingType(prop, gbSpending, hInst);
	SendMessage(hSpendingType, WM_SETFONT, WPARAM(hFont), TRUE); //combobox loại chi tiêu

	prop = { 200, 50, 150, 25, NULL };
	hDetail = mHandle->GetHandleDetails(prop, gbSpending, hInst); //text box chi tiết chi tiêu
	SendMessage(hDetail, WM_SETFONT, WPARAM(hFont), TRUE);

	prop = { 370, 50, 100, 25, NULL };
	hCost = mHandle->GetHandleCost(prop, gbSpending, hInst); //text box giá tiền
	SendMessage(hCost, WM_SETFONT, WPARAM(hFont), TRUE);

	prop = { 490, 50, 60, 25, (HMENU)IDC_MYADDBUTTON };
	AddBut = mHandle->GetHandleAddButton(prop, gbSpending, hInst); //button box thêm
	SendMessage(AddBut, WM_SETFONT, WPARAM(hFont), TRUE);

	wndProcSpeding = (WNDPROC)SetWindowLongPtr(gbSpending, GWLP_WNDPROC, (LONG_PTR)&GBSpendingWndProc);
}

void IncludeListSpendingControls(HWND hWnd)
{
	HFONT hFontTotal = CreateFont(25, 0, 0, 0, 800, FALSE, FALSE, FALSE, VIETNAMESE_CHARSET,
		OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Calibri"));

	gbSpendingList = CreateWindowEx(0, L"Button", L"Danh sách chi tiêu", WS_CHILD | WS_VISIBLE | BS_GROUPBOX,
		10, 170, 600, 430, hWnd, NULL, hInst, NULL);
	SendMessage(gbSpendingList, WM_SETFONT, WPARAM(hFont), TRUE); //khung chứa danh sách là 1 list view

	HandleProperties prop = { 20, 30, 550, 330, NULL };
	lvSpending = mHandle->GetHandleSpendingListview(prop, gbSpendingList, hInst);
	SendMessage(lvSpending, WM_SETFONT, WPARAM(hFont), TRUE);

	HWND text = CreateWindow(L"STATIC", L"Tổng cộng:", WS_CHILD | WS_VISIBLE,
		200, 380, 100, 24, gbSpendingList, NULL, hInst, NULL);
	SendMessage(text, WM_SETFONT, WPARAM(hFontTotal), TRUE);

	prop = { 350, 380, 200, 24, NULL };
	hTotalAmount = mHandle->GetHandleTotalAmount(prop, gbSpendingList, hInst);
	SendMessage(hTotalAmount, WM_SETFONT, WPARAM(hFontTotal), TRUE);

	wndProcListview = (WNDPROC)SetWindowLongPtr(gbSpendingList, GWLP_WNDPROC, (LONG_PTR)&GBListviewWndProc);
}

void IncludePieChartControls(HWND hwnd)
{
	HandleProperties prop = { 630, 10, 500, 600, NULL };
	gbPieChart = CreateWindowEx(0, L"Button", L"Biểu đồ", WS_CHILD | WS_VISIBLE | BS_GROUPBOX,
		prop.x, prop.y, prop.width, prop.height, hwnd, prop.hMenu, hInst, NULL);
	SendMessage(gbPieChart, WM_SETFONT, WPARAM(hFont), TRUE);

	int radius = 140;
	Coord origin = { prop.x + prop.width / 2, prop.y + radius + 60 };
	mChart = new Chart(origin, radius);
	mChart->UpdateCoord(mHandle->GetListSpendingType());
}

void ClearAllFields()
{
	SendMessage(hSpendingType, CB_SETCURSEL, -1, 0);
	SetWindowText(hDetail, L"");
	SetWindowText(hCost, L"");
}
