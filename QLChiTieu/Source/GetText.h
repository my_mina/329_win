﻿#pragma once
#include <string>

using namespace std;

/******************Lấy chuỗi text của đối tượng chi tiêu********************/
class GetText
{
public:
	GetText();
	static wstring GetComboboxText(HWND hWnd, int index);
	static wstring GetDetailText(HWND hWnd);
	static int GetCostText(HWND hWnd);
	~GetText();
};
