#include "stdafx.h"
#include "FileStream.h"
#include <locale>
#include <codecvt>
#include <sstream>


FileStream::FileStream()
{
}

FileStream::FileStream(string _filename)
{
	this->filename = _filename;
}

FileStream::~FileStream()
{
}

vector<Spending> FileStream::ReadFile()
{
	vector<Spending> vSpend;
	locale loc(locale(), new codecvt_utf8<wchar_t>);
	wifstream ifs(this->filename);
	ifs.imbue(loc);

	if (ifs.is_open())
	{
		while (!ifs.eof())
		{
			wstring buffer, item;
			getline(ifs, buffer);

			if (!buffer.empty())
			{
				wstringstream ss(buffer);
				vector<wstring> items;

				while (getline(ss, item, L';')) items.push_back(item);
				Spending spd(items[0], items[1], stoi(items[2]));
				vSpend.push_back(spd);
			}
			else break;
		}
	}

	ifs.close();

	return vSpend;
}

void FileStream::SaveFile(vector<Spending> vSpend)
{
	locale loc(locale(), new codecvt_utf8<wchar_t>);
	wofstream ofs(this->filename);
	ofs.imbue(loc);

	for (int i = 0; i < vSpend.size(); ++i)
	{
		wstring buffer = vSpend[i].GetSpdType() + L';' +
			vSpend[i].GetSpdDetail() + L';' +
			to_wstring(vSpend[i].GetSpdCost());

		ofs << buffer;
		if (i < vSpend.size() - 1) ofs << endl;
	}
	ofs.close();
}
