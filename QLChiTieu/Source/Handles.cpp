﻿#include "stdafx.h"
#include "Handles.h"
#include <CommCtrl.h>
#include "resource.h"

#pragma comment(lib, "comctl32.lib")

vector<wstring> TypeSamples = {
	L"Ăn uống",
	L"Di chuyển",
	L"Nhà cửa",
	L"Xe cộ",
	L"Nhu yếu phẩm",
	L"Dịch vụ"
};

Handles::Handles()
{
	this->hSpendingType = NULL;
	this->hDetail = NULL;
	this->AddBut = NULL;

	this->lvSpending = NULL;
	this->hTotalAmount = NULL;
	this->hCost = 0;
}

Handles::Handles(vector<Spending> _vSpd)
{
	this->hSpendingType = NULL;
	this->hDetail = NULL;
	this->AddBut = NULL;

	this->lvSpending = NULL;
	this->hTotalAmount = NULL;
	this->hCost = 0;

	this->vSpd = _vSpd;

	//tạo vector với phàn tử là loại chi tiêu ứng với mẫu, khởi tạo tổng tiền mỗi loại là 0
	for (int i = 0; i < TypeSamples.size(); ++i)
	{
		SpendingType type = { TypeSamples[i], 0 };
		this->vType.push_back(type);
	}
	//tổng tiền của mỗi loại
	for (int i = 0; i < this->vSpd.size(); ++i)
	{
		this->PushSpendingType(this->vSpd[i]);
	}
}
Handles::~Handles()
{
}

HWND Handles::GetHandleSpendingType(HandleProperties prop, HWND hWndParent, HINSTANCE hInst)
{
	if (this->hSpendingType == NULL)
	{
		this->hSpendingType = CreateWindowEx(0, L"COMBOBOX", L"MyCombo", CBS_DROPDOWNLIST | WS_CHILD | WS_VISIBLE,
			prop.x, prop.y, prop.width, prop.height, hWndParent, prop.hMenu, hInst, NULL);

		for (int i = 0; i < this->vType.size(); ++i) //Add string to combobox.
		{
			SendMessage(this->hSpendingType, CB_ADDSTRING,
				0, LPARAM(this->vType.at(i).name.c_str()));
		}
	}

	return this->hSpendingType;
}

HWND Handles::GetHandleDetails(HandleProperties prop, HWND hWndParent, HINSTANCE hInst)
{
	if (this->hDetail == NULL)
	{
		this->hDetail = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER,
			prop.x, prop.y, prop.width, prop.height, hWndParent, prop.hMenu, hInst, NULL);
		SendMessage(this->hDetail, EM_SETCUEBANNER, 0, LPARAM(L"Nội dung"));
	}

	return this->hDetail;
}

HWND Handles::GetHandleCost(HandleProperties prop, HWND hWndParent, HINSTANCE hInst)
{
	if (this->hCost == NULL)
	{
		this->hCost = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER,
			prop.x, prop.y, prop.width, prop.height, hWndParent, prop.hMenu, hInst, NULL);
		SendMessage(this->hCost, EM_SETCUEBANNER, 0, LPARAM(L"Số tiền"));
	}

	return this->hCost;
}

HWND Handles::GetHandleAddButton(HandleProperties prop, HWND hWndParent, HINSTANCE hInst)
{
	if (this->AddBut == NULL)
	{
		this->AddBut = CreateWindowEx(0, L"BUTTON", L"Thêm", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
			prop.x, prop.y, prop.width, prop.height, hWndParent, prop.hMenu, hInst, NULL);
	}

	return this->AddBut;
}

HWND Handles::GetHandleSpendingListview(HandleProperties prop, HWND hWndParent, HINSTANCE hInst)
{
	InitCommonControls();
	if (this->lvSpending == NULL)
	{
	this->lvSpending = CreateWindowEx(WS_EX_CLIENTEDGE, WC_LISTVIEW, _T("List View"),
		WS_CHILD | WS_VISIBLE | WS_VSCROLL | LVS_EDITLABELS | LVS_SHOWSELALWAYS
			| WS_BORDER | LVS_REPORT ,
			prop.x, prop.y, prop.width, prop.height, hWndParent, prop.hMenu, hInst, NULL);

		SendMessage(this->lvSpending, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_FULLROWSELECT);

		LVCOLUMN lvCol;

		lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
		lvCol.fmt = LVCFMT_LEFT;

		lvCol.cx = 130;
		lvCol.pszText = _T("Loại chi tiêu");
		ListView_InsertColumn(this->lvSpending, 0, &lvCol);

		lvCol.fmt = LVCFMT_LEFT | LVCF_WIDTH;
		lvCol.cx = 290;
		lvCol.pszText = _T("Nội dung");
		ListView_InsertColumn(this->lvSpending, 1, &lvCol);

		lvCol.fmt = LVCFMT_RIGHT;
		lvCol.cx = 100;
		lvCol.pszText = _T("Số tiền");
		ListView_InsertColumn(this->lvSpending, 2, &lvCol);
		
		for (int i = 0; i < this->vSpd.size(); ++i)
		{
			LV_ITEM lv;
			//Nạp cột đầu tiên loại chi tiêu
			lv.mask = LVIF_TEXT | LVIF_PARAM;
			lv.iItem = i;

			lv.iSubItem = 0;
			lv.pszText = new WCHAR[this->vSpd[i].GetSpdType().length() + 1];
			wcscpy(lv.pszText, this->vSpd[i].GetSpdType().c_str());
			ListView_InsertItem(this->lvSpending, &lv);

			lv.mask = LVIF_TEXT;
			lv.iSubItem = 1;
			lv.pszText = new WCHAR[this->vSpd[i].GetSpdDetail().length() + 1];
			wcscpy(lv.pszText, this->vSpd[i].GetSpdDetail().c_str());
			ListView_SetItem(this->lvSpending, &lv);

			lv.mask = LVIF_TEXT;
			lv.iSubItem = 2;
			_itow(this->vSpd[i].GetSpdCost(), lv.pszText, 10);
			ListView_SetItem(this->lvSpending, &lv);
		}
	}
	return this->lvSpending;
}

HWND Handles::GetHandleTotalAmount(HandleProperties prop, HWND hWndParent, HINSTANCE hInst)
{
	if (this->hTotalAmount==NULL)
	{

		this->hTotalAmount = CreateWindowEx(0, L"STATIC", L"", WS_CHILD | WS_VISIBLE,
			prop.x, prop.y, prop.width, prop.height, hWndParent, prop.hMenu, hInst, NULL);
		this->UpdateSpending();
	}

	return this->hTotalAmount;
}

vector<Spending> Handles::GetSpendingList()
{
	return this->vSpd;
}

vector<SpendingType> Handles::GetListSpendingType()
{
	return this->vType;
}
void Handles::AddSpendingItem(Spending spd)
{
	this->vSpd.push_back(spd);

	LVITEM lv;

	lv.mask = LVIF_TEXT;
	lv.iItem = this->vSpd.size() - 1;
	lv.iSubItem = 0;
	lv.pszText = new WCHAR[spd.GetSpdType().length() + 1];
	wcscpy(lv.pszText, spd.GetSpdType().c_str());
	ListView_InsertItem(this->lvSpending, &lv);

	lv.mask = LVIF_TEXT;
	lv.iSubItem = 1;
	lv.pszText = new WCHAR[spd.GetSpdDetail().length() + 1];
	wcscpy(lv.pszText, spd.GetSpdDetail().c_str());
	ListView_SetItem(this->lvSpending, &lv);

	lv.mask = LVIF_TEXT;
	lv.iSubItem = 2;
	_itow(spd.GetSpdCost(), lv.pszText, 10);
	ListView_SetItem(this->lvSpending, &lv);

	this->UpdateSpending();
	this->UpdateSpendingType();
}

void Handles::PushSpendingType(Spending spd)
{
	for (int i = 0; i < this->vType.size(); ++i)
	{
		if (spd.GetSpdType() == this->vType[i].name)
		{
			this->vType[i].totalCost = this->vType[i].totalCost + spd.GetSpdCost();
			return;
		}
	}
}

bool Handles::DeleteSpendingItem(int index)
{
	if (index >= this->vSpd.size() || index < 0) return false;
	else
	{
		this->vSpd.erase(vSpd.begin() + index);
		ListView_DeleteItem(this->lvSpending, index);
		this->UpdateSpending();
		this->UpdateSpendingType();
		return true;
	}

	ListView_DeleteItem(this->lvSpending, index);
}

void Handles::UpdateSpending()
{
	int total = 0;
	for (int i = 0; i < this->vSpd.size(); ++i)
	{
		total += this->vSpd[i].GetSpdCost();
	}

	wstring result = to_wstring(total);
	SetWindowText(this->hTotalAmount, result.c_str());
}

void Handles::UpdateSpendingType()
{
	for (int i = 0; i < this->vType.size(); ++i)
	{
		this->vType[i].totalCost = 0;
		for (int j = 0; j < this->vSpd.size(); ++j)
		{
			if (this->vType[i].name == this->vSpd[j].GetSpdType())
			{
				this->vType[i].totalCost = this->vType[i].totalCost + this->vSpd[j].GetSpdCost();
			}
		}
	}
}