#include "stdafx.h"
#include "Spending.h"


Spending::Spending()
{
}

Spending::Spending(wstring type, wstring detail, int cost)
{
	this->spdType = type;
	this->spdDetail = detail;
	this->spdCost = cost;
}

wstring Spending::GetSpdType() {return this->spdType;}
wstring Spending::GetSpdDetail() {return this->spdDetail;}
int Spending::GetSpdCost() {return this->spdCost;}

void Spending::SetSpdType(wstring type) {this->spdType = type;}
void Spending::SetSpdDetail(wstring detail) {this->spdDetail = detail;}
void Spending::SetSpdCost(int cost) {this->spdCost = cost;}

Spending::~Spending()
{}
