﻿#pragma once
#include "Structs.h"
#include <vector>

/*****************Lớp đồ thị*********************/
class Chart
{
private:
	Coord origin; //tâm
	int radius; //bán kính
	vector<SpdTypeInChart> listType; //danh sách các loại chi tiêu
	float GetSweepAngle(int);
	//Coord GetCoordBtwTwoPoints(Coord p1, Coord p2, float percent);
public:
	Chart();
	Chart(Coord, int);
	void Draw(HDC hdc); //vẽ chart
	void UpdateCoord(vector<SpendingType>); //cập nhật lại tọa độ
	void ShowNote(HDC hdc); //chú thích
	~Chart();
};

