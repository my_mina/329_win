﻿#ifndef __STRUCTS_H__
#define __STRUCTS_H__

#include <string>

/*************Chứa các struct mới được tạo để sử dụng*************/

using namespace std;

struct SpendingType
{
	wstring name;
	int totalCost;
};
//tọa độ
struct Coord {int x, y;};

//màu sắc
struct RGBColor {int r, g, b;};

//loại chi tiêu đc biểu diễn trong biểu đồ
struct SpdTypeInChart
{
	wstring name;
	int amount;
	RGBColor color;
	float xStartAngle;//điểm bắt đầu của 1 cung
	float xSweepAngle; //độ lớn của 1 cung
};

struct HandleProperties
{
	int x, y, width, height;
	HMENU hMenu;
};

#endif