﻿#pragma once

#include "resource.h"
#include "Handles.h"
#include <vector>
#include <string>
#include "Chart.h"
#include "FileStream.h"

#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

//biến cục bộ
HWND m_hWnd;
int wmId, wmEvent;

Handles *mHandle = NULL;
Chart *mChart = NULL;
FileStream *mFile = NULL;
HFONT hFont = NULL;

HWND gbSpending = NULL;
HWND gbSpendingList = NULL;
HWND gbPieChart = NULL;

HWND hSpendingType = NULL;
HWND hDetail = NULL;
HWND hCost = NULL;
HWND AddBut = NULL;

HWND lvSpending = NULL;
HWND hTotalAmount = NULL;

HMENU hPopupMenu;

WNDPROC wndProcSpeding;
WNDPROC wndProcListview;

int lvSelectID = -1;
Spending mSelectSpd;

HDC hdc;

//hàm
LRESULT CALLBACK	GBSpendingWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK	GBListviewWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

void				IncludeAddSpendingControls(HWND hWnd);
void				IncludeListSpendingControls(HWND hWnd);
void				IncludePieChartControls(HWND hWnd);
void				ClearAllFields();
void				ShowEditWindow();
void				ShowEditCommonControls();
void				ShowEditWindow();
