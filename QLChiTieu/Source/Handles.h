﻿#pragma once
#include "Spending.h"
#include <vector>
#include <string>
#include "Structs.h"

using namespace std;

/******************Điều khiển các cửa sổ thêm chi tiêu, list view, tổng cộng********************/
class Handles
{
private:
	vector<Spending> vSpd; //vector quản lý các chi tiêu
	vector<SpendingType> vType; //vector quản lý các loại chi tiêu

	HWND hSpendingType, hDetail, hCost, AddBut, hTotalAmount;
	HWND lvSpending;
public:
	Handles();
	Handles(vector<Spending> _vSpd);
	~Handles();

	HWND GetHandleSpendingType(HandleProperties prop, HWND hWndParent, HINSTANCE hInst);
	HWND GetHandleDetails(HandleProperties prop, HWND hWndParent, HINSTANCE hInst);
	HWND GetHandleCost(HandleProperties prop, HWND hWndParent, HINSTANCE hInst);
	HWND GetHandleAddButton(HandleProperties prop, HWND hWndParent, HINSTANCE hInst);
	HWND GetHandleSpendingListview(HandleProperties prop, HWND hWndParent, HINSTANCE hInst);
	HWND GetHandleTotalAmount(HandleProperties prop, HWND hWndParent, HINSTANCE hInst);

	vector<Spending> GetSpendingList();
	vector<SpendingType> GetListSpendingType();

	void AddSpendingItem(Spending spd);
	void PushSpendingType(Spending spd);
	bool DeleteSpendingItem(int index);
	void UpdateSpending();
	void UpdateSpendingType();
};

