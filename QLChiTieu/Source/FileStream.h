#pragma once
#include <string>
#include <vector>
#include <fstream>
#include "Spending.h"

using namespace std;

class FileStream
{
private:
	string filename;
public:
	FileStream();
	FileStream(string _filename);
	~FileStream();

	vector<Spending> ReadFile();
	void SaveFile(vector<Spending> vCat);
};

