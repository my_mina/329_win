﻿#pragma once
#include <string>

using namespace std;

/**************Lớp chi tiêu*******************/
class Spending
{
private:
	wstring spdType; //loại chi tiêu
	wstring spdDetail; //chi tiết về chi tiêu
	int spdCost; //giá cả
public:
	Spending();
	Spending(wstring, wstring, int);

	wstring GetSpdType();
	wstring GetSpdDetail();
	int GetSpdCost();

	void SetSpdType(wstring);
	void SetSpdDetail(wstring);
	void SetSpdCost(int);
	~Spending();
};

